#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

#include <QDateTime>
#include "picparse.h"
#include "smartcap.h"
#include "settings/smartsettings.h"

using namespace std;
using namespace cv;

/** Function Headers */
void detectAndDisplay( Mat frame );

/** Global variables */
string window_name = "Capture - Face detection";
RNG rng(12345);

#define PI 3.14159265

cv::Point RotatePoint(cv::Point center, cv::Point point, double angle)
{
    cv::Point rez;
    float s = sin(angle*PI/180);
    float c = cos(angle*PI/180);
    point.x -= center.x;
    point.y -= center.y;
    rez.x = (float)point.x * c - (float)point.y * s;
    rez.y = (float)point.x * s + (float)point.y * c;
    rez.x += center.x;
    rez.y += center.y;
    return rez;
}

cv::Rect ContainedRotatedRect(cv::Rect source, cv::Point center, double angle)
{
    cv::Point tP;
    int minx,miny,maxx,maxy;
    tP.x = source.x;
    tP.y = source.y;
    tP = RotatePoint(center,tP,angle);
    minx = maxx = tP.x;
    miny = maxy = tP.y;

    tP.x = source.x+source.width;
    tP.y = source.y+source.height;
    tP = RotatePoint(center,tP,angle);
    minx = std::min(tP.x,minx);
    maxx = std::max(tP.x,maxx);
    miny = std::min(tP.y,miny);
    maxy = std::max(tP.y,maxy);

    tP.x = source.x;
    tP.y = source.y+source.height;
    tP = RotatePoint(center,tP,angle);
    minx = std::min(tP.x,minx);
    maxx = std::max(tP.x,maxx);
    miny = std::min(tP.y,miny);
    maxy = std::max(tP.y,maxy);

    tP.x = source.x+source.width;
    tP.y = source.y;
    tP = RotatePoint(center,tP,angle);
    minx = std::min(tP.x,minx);
    maxx = std::max(tP.x,maxx);
    miny = std::min(tP.y,miny);
    maxy = std::max(tP.y,maxy);

    return cv::Rect(minx,miny,maxx-minx,maxy-miny);
}

cv::Mat rotatedFace(cv::Mat src, cv::Rect region, double angle){
    int maxSizeLarge = std::sqrt(std::pow(region.size().width,2)+std::pow(region.size().height,2));
    maxSizeLarge = std::sqrt(std::pow(maxSizeLarge,2)*2);

    cv::Point regionCenter(region.x+region.width/2, region.y+region.height/2);
    if(regionCenter.x-maxSizeLarge/2 < 0){
        maxSizeLarge = maxSizeLarge+(regionCenter.x-maxSizeLarge/2)*2;
    }
    if(regionCenter.y-maxSizeLarge/2 < 0){
        maxSizeLarge = maxSizeLarge+(regionCenter.y-maxSizeLarge/2)*2;
    }
    if(regionCenter.x+maxSizeLarge/2 > src.size().width){
        maxSizeLarge = maxSizeLarge-(src.size().width - (regionCenter.x+maxSizeLarge/2 ))*2;
    }
    if(regionCenter.y+maxSizeLarge/2 > src.size().height){
        maxSizeLarge = maxSizeLarge-(src.size().height - (regionCenter.y+maxSizeLarge/2 ))*2;
    }

    cv::Rect largeMatRect(regionCenter.x-maxSizeLarge/2, regionCenter.y-maxSizeLarge/2,maxSizeLarge,maxSizeLarge);
    cv::Point largeMatRectCenter(maxSizeLarge/2, maxSizeLarge/2);

    cv::Mat largeDst(largeMatRect.size(),src.type());
    src(largeMatRect).copyTo(largeDst);

    /** Rotating the image after Warp */
    cv::Mat rot_mat( 2, 3, src.type());
    /// Get the rotation matrix with the specifications above
    rot_mat = getRotationMatrix2D( largeMatRectCenter, angle, 1 );
     /// Rotate the image
    warpAffine( largeDst, largeDst, rot_mat, largeDst.size() );

    cv::Rect smallRect = ContainedRotatedRect(region,regionCenter,angle);


    smallRect.x = (largeMatRect.size().width - smallRect.size().width)/2;
    smallRect.y = (largeMatRect.size().height - smallRect.size().height)/2;
    cv::rectangle( largeDst, smallRect, cv::Scalar( 125 ));

    smallRect.x = std::max(smallRect.x,0);
    smallRect.y = std::max(smallRect.y,0);
    smallRect.width = std::min(smallRect.width,largeDst.size().width - smallRect.x);
    smallRect.height = std::min(smallRect.height,largeDst.size().height - smallRect.x);

    cv::Mat dst(smallRect.size(),src.type());
    largeDst(smallRect).copyTo(dst);
    return dst;
}


/** @function main */
int main( int argc __attribute__ ((unused)), const char** argv __attribute__ ((unused)))
{
    Mat * frame = 0;
    SmartSettings settings;
    PicParse parser(settings,true);

    //SmartCap smartCap("/home/rotjix/data/test.avi");
    //SmartCap smartCap(FLYCAP_CAP,0);
    SmartCap smartCap(EASY_CAP,-1);
    QDir dumpdir = QDir(QString("%1/out/").arg(QDir::current().absolutePath()));

    quint64 started = QDateTime::currentDateTime().toMSecsSinceEpoch();
    quint64 processed = 0;

    cv::Mat rotated;
    cv::Rect cutout(20,100,300,200);

    if( smartCap.ok() )
    {
        int i  = 0;
        while( true )
        {
            smartCap.prepareFrame();
            frame = smartCap.getFrame();

            //-- 3. Apply the classifier to the frame
            if( !frame->empty() ){
                //parser.ParseFaces(*frame);
                processed++;
                qDebug() << "=== FPM: " << i++ << " == " << (( (float)processed/(QDateTime::currentDateTime().toMSecsSinceEpoch()-started)*1000*60));
                //cv::rectangle(*frame,cutout,cv::Scalar(0,0,0));

                //rotated = rotatedFace(*frame,cutout,30);
                /*
                parser.DumpFaces(
                    QDir(QString("%1/out/").arg(QDir::current().absolutePath())),
                    QString("face_%1_%2.jpg").arg(QDateTime::currentDateTime().toMSecsSinceEpoch()).arg("%1"));
                */
                //imwrite( dumpdir.filePath(QString("face_%1.jpg").arg((int)i++,4,10,QLatin1Char( '0' )).toStdString().c_str()).toStdString().c_str(), *frame );

                imshow( window_name, *frame );
                //imshow( "rot", rotated );
            }
            else
            {
                printf(" --(!) No captured frame -- Break!\n");
                //break;
                smartCap.init();
            }

            int c = waitKey(10);
            delete frame;
            if( (char)c == 'c' ) { break; }
        }
    }
    printf("Done\n");
    return 0;
}

