#include "wizzard.h"
#include <QApplication>
#include <QSettings>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("Applyit");
    QCoreApplication::setOrganizationDomain("applyit.lv");
    QCoreApplication::setApplicationName("SMBWizzard");

    //QObject::connect(&wsmbconnect,SIGNAL(on_btnPingL_clicked()),&lanscanner,SLOT(startScan()));
    Wizzard w;
    w.show();

    return a.exec();
}
