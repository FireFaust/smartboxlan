#include "wtvreg.h"
#include "ui_wtvreg.h"

WTvReg::WTvReg(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WTvReg)
{
    smarttvRectPack = 0;
    authManager = 0;
    timer = new QTimer(this);
    manager = new QNetworkAccessManager(this);
    scene = new RectSelectScene(this);
    recScene = new QGraphicsScene(this);

    capturing = false;
    recordingFrames = false;

    connect(timer,SIGNAL(timeout()),this,SLOT(grabFrame()));

    ui->setupUi(this);

  //  ui->btnSubmit->setEnabled(false);
    ui->preview->setScene(scene);
    ui->recPreview->setScene(recScene);

    connect(this->manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(grabFrameFinished(QNetworkReply *)));
    connect(ui->btnStartStop, SIGNAL(clicked()), this, SLOT(btnStartStopClick()));
    connect(ui->btnSubmit, SIGNAL(clicked()), this, SLOT(btnSubmitClick()));
    connect(ui->btnCancel, SIGNAL(clicked()), this, SLOT(btnCancelClick()));
    connect(ui->btnRecord, SIGNAL(clicked()), this, SLOT(btnRecordClick()));
    connect(ui->btnClearLeft, SIGNAL(clicked()), this, SLOT(btnClearLeftClick()));
    connect(ui->btnClearRight, SIGNAL(clicked()), this, SLOT(btnClearRightClick()));
    connect(ui->frameSlider, SIGNAL(valueChanged(int)),this,SLOT(sliderValueChanged(int)));

    status = Capturing;
    mode = Record;
}

WTvReg::~WTvReg()
{
    delete ui;
}

void WTvReg::setManagerAuth(QNetworkAccessManager *manager)
{
    this->authManager = manager;
}

void WTvReg::btnStartStopClick()
{
    if(capturing){
        stopCapture();
    }else{
        startCapture();
    }
    formatButtons();
}

void WTvReg::btnSubmitClick()
{
    if(authManager == 0) return;
    QRectF rect = scene->getRect();

    ui->btnStartStop->setEnabled(false);
    ui->btnSubmit->setEnabled(false);

    std::vector<QByteArray> vframes;
    vframes.insert(vframes.begin(),frames.begin(),frames.end());

    smarttvRectPack = new SmartTvRecPack(authManager,postPath,vframes,rect,scene->getPicRect(),this);
    connect(smarttvRectPack,SIGNAL(finished(SmartTvRecPack*)),this,SLOT(smartFacePackFinished(SmartTvRecPack*)));
    connect(smarttvRectPack,SIGNAL(failed(SmartTvRecPack*)),this,SLOT(smartFacePackFailed(SmartTvRecPack*)));
    smarttvRectPack->send();
}


void WTvReg::startTvReg(QString postPath, QString cancelLink, TvRegMode mode, QRectF rect)
{
    if(rect.x()<0)rect.setX(0);
    if(rect.y()<0)rect.setY(0);
    if(rect.width()<10)rect.setWidth(10);
    if(rect.height()<10)rect.setHeight(10);

    this->mode = mode;
    this->cancelLink = cancelLink;
    this->postPath = postPath;
    status = Capturing;
    scene->setCanResize(mode == Mark);
    scene->setRect(rect,true);
    frames.clear();
    startCapture();
    formatButtons();
}

void WTvReg::startCapture()
{
    if(capturing) return;
    timer->start(1000/12);
    capturing = true;
}

void WTvReg::stopCapture()
{
    if(!capturing) return;
    timer->stop();
    capturing = false;
    ui->btnStartStop->setText(tr("Resume"));
}


void WTvReg::grabFrame()
{
    if(manager == 0){
        qWarning() << "WLearn QNetworkAccessManager not set!";
        return;
    }
    QUrl url(QString("http://").append(connection.getIpPort().append("/grab/tv")));
    QNetworkRequest request(url);
    QNetworkReply * reply = manager->get(request);
    reply->setParent(this);
}

void WTvReg::grabFrameFinished(QNetworkReply * reply)
{
    if(reply->error() != QNetworkReply::NoError){
        qDebug() << reply->errorString();
        return;
    }
    frameGrabbed(reply->readAll());
    reply->deleteLater();
}


void WTvReg::frameGrabbed(QByteArray jpg)
{
    if(capturing == false) return;

    if(status == Recording)
    {
        QRectF rect = scene->getRect();
        QByteArray * cropped = 0;
        cropped = PicParse::getPartJpeg(&jpg,rect.x(),rect.y(),rect.width(),rect.height());
        frames.push_back(*cropped);
        delete cropped;

    }
    showJpg(jpg);
}

void WTvReg::showJpg(QByteArray jpg)
{
    scene->setImage(jpg);
}

void WTvReg::smartFacePackFinished(SmartTvRecPack * smarttvRectPack)
{
    emit finishedTvRect(smarttvRectPack->getRedirectLink());
    smarttvRectPack->deleteLater();
    timer->stop();
    capturing = false;
    this->smarttvRectPack = 0;
}

void WTvReg::smartFacePackFailed(SmartTvRecPack * smarttvRectPack)
{
    smarttvRectPack->deleteLater();
    this->smarttvRectPack = 0;
}

void WTvReg::btnCancelClick()
{
    if(smarttvRectPack){
        smarttvRectPack->stop();
        smarttvRectPack->deleteLater();
        smarttvRectPack = 0;
        formatButtons();
    }else{
        timer->stop();
        capturing = false;
        emit finishedTvRect(cancelLink);
    }
}


void WTvReg::btnRecordClick()
{
    if(status == Capturing){
        status = Recording;
    }else if(status == Recording){
        if(frames.empty()){
            status = Capturing;
        }else{
            status = Editing;
            stopCapture();
            formatButtons();
            ui->frameSlider->setValue(0);
            showRecJpg(frames[0]);
        }
    }else if(status == Editing){
        frames.clear();
        status = Capturing;
        startCapture();
    }
    formatButtons();
}

void WTvReg::btnClearLeftClick()
{
    int value = ui->frameSlider->value();
    if(frames.size()<=1) return;
    if((int)frames.size() <= value ) return;
    std::deque<QByteArray>::iterator curr = frames.begin()+value;
    frames.erase(frames.begin(),curr);
    formatButtons();
    ui->frameSlider->setValue(0);
}
void WTvReg::btnClearRightClick()
{
    int value = ui->frameSlider->value();
    if(frames.size() <= 1) return;
    if((int)frames.size() <= value + 1 ) return;
    std::deque<QByteArray>::iterator curr = frames.begin()+value;
    frames.erase(++curr,frames.end());
    formatButtons();
    ui->frameSlider->setValue(frames.size()-1);
}

void WTvReg::sliderValueChanged(int value)
{
    if(frames.empty()) return;
    if((int)frames.size() <= value ) return;
    showRecJpg(frames[value]);
    formatButtons();
}

void WTvReg::showRecJpg(QByteArray jpg)
{
    if(jpg.isEmpty()) return;
    recScene->clear();
    QImage image;
    image = QImage::fromData(jpg,"jpg");
    recScene->addPixmap(QPixmap::fromImage(image));
    ui->recPreview->setTransform(QTransform());
    qreal scale;
    if(ui->recPreview->width()/ui->recPreview->height() >
            recScene->width()/recScene->height()){
        scale = ui->recPreview->height()/ recScene->height();
    }else{
        scale = ui->recPreview->width()/ recScene->width();
    }
    scale *= 0.95;
    ui->recPreview->scale(scale,scale);
}

void WTvReg::formatButtons()
{
    ui->frameSlider->setMaximum(frames.size()-1);
    ui->frameSlider->setMinimum(0);

    ui->recPreview->setVisible(mode == Record && status==Editing);
    ui->preview->setVisible(mode == Mark || status!=Editing);

    ui->btnSubmit->setEnabled((!(recordingFrames||capturing) && (mode == Record &&!frames.empty())) ||(mode == Mark) );

    ui->btnStartStop->setVisible(mode == Mark);
    ui->btnRecord->setVisible(mode == Record);
    ui->btnClearLeft->setVisible(mode == Record);
    ui->btnClearRight->setVisible(mode == Record);

    ui->btnClearLeft->setEnabled(status == Editing && !(frames.size() <= (unsigned int)ui->frameSlider->value()) && ui->frameSlider->value() > 0  );
    ui->btnClearRight->setEnabled(status == Editing  && !(frames.size() <= (unsigned int)ui->frameSlider->value() + 1) );

    ui->btnNextFrame->setVisible(status == Editing);
    ui->btnPrevFrame->setVisible(status == Editing);
    ui->frameSlider->setVisible(status == Editing);

    if(capturing){
        ui->btnStartStop->setText(tr("Pause"));
    }else{
        ui->btnStartStop->setText(tr("Start"));
    }

    ui->btnStartStop->setEnabled(mode == Mark);

    if(status == Recording){
        ui->btnRecord->setText(tr("Stop"));
    }else if(status == Capturing){
        ui->btnRecord->setText(tr("Record"));
    }else if(status == Editing){
        ui->btnRecord->setText(tr("Clear"));
    }
}
