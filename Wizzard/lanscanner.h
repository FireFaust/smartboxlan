#ifndef LANSCANNER_H
#define LANSCANNER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QUrl>
#include <QByteArray>
#include <QNetworkInterface>
#include <QTimer>
#include <QHostAddress>

class LanScanner : public QObject
{
    Q_OBJECT
public:
    explicit LanScanner(QObject *parent = 0);

signals:
    void foundSmartBox(QString address);
    void complitedScan(int i);
public slots:
    void startScan();
protected slots:
    void onRequestCompleted();
private:
    QString minMaxIP(QString ip, QString mask, int minmax);
    bool ifSmbox(QByteArray str);
    void complitedScanFunc();
    void getRequest(QNetworkRequest request);
    void BigSender();
    QNetworkAccessManager *manager;
    QNetworkReply *reply;
    int min[4],min_D[4],max_D[4],max[4];
    int counter;
};
class QReplyTimeout : public QObject {
  Q_OBJECT
public:
  QReplyTimeout(QNetworkReply* reply, const int timeout) : QObject(reply) {
    Q_ASSERT(reply);
    if (reply) {
      QTimer::singleShot(timeout, this, SLOT(timeout()));
    }
  }
private:
private slots:
  void timeout() {
    QNetworkReply* reply = static_cast<QNetworkReply*>(parent());
    if (reply->isRunning()) {
      reply->abort();
    }
  }
};
#endif // LANSCANNER_H
