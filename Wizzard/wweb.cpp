#include "wweb.h"
#include "ui_wweb.h"

WWeb::WWeb(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WWeb)
{
    ui->setupUi(this);
    connect(ui->webView,SIGNAL(loadFinished(bool)),SLOT(loadFinished(bool)));
    webPage = new SmartWebPage(ui->webView);
    manager = new QNetworkAccessManager(ui->webView);
    webPage->setNetworkAccessManager(manager);
    ui->webView->setPage(webPage);
    ui->webView->load(QUrl(settings.value("Wizzard/serverDomain","http://192.168.4.142:8080").toString()
                           .append(settings.value("Wizzard/startPath","/OtwGateway/wizard/device/registration/residence").toString())));

    ui->webView->setStyleSheet("background:transparent");
    ui->webView->setAttribute(Qt::WA_TranslucentBackground);
}

WWeb::~WWeb()
{
    delete ui;
}

void WWeb::loadFinished(bool ok)
{
    qDebug() << " Loaded " << ui->webView->url().toString() << " " << ok;
    if(ui->webView->url().toString().endsWith("client:native")){
        qDebug() << ui->webView->page()->mainFrame()->toPlainText();
        QJsonDocument doc = QJsonDocument::fromJson(ui->webView->page()->mainFrame()->toPlainText().toUtf8());
        QJsonObject jsonObj = doc.object();
        QString clientId = QString::number((unsigned int)jsonObj.value("clientId").toDouble());
        QString postPath = jsonObj.value("submitLink").toString();
        QString cancelLink = settings.value("Wizzard/serverDomain","http://192.168.4.142:8080").toString()
                .append(settings.value("Wizzard/startPath","/OtwGateway/wizard/device/registration/residence").toString());
        if(jsonObj.contains("cancelLink")){
            cancelLink = jsonObj.value("cancelLink").toString();
        }
        emit launchLearn(clientId, cancelLink, postPath);
        return;
    }else if(ui->webView->url().toString().endsWith("channel:native")){
        qDebug() << ui->webView->page()->mainFrame()->toPlainText();
        QJsonDocument doc = QJsonDocument::fromJson(ui->webView->page()->mainFrame()->toPlainText().toUtf8());
        QJsonObject jsonObj = doc.object();
        QRect rect(100,50,10,50);
        if(jsonObj.contains("rect")){
            QJsonObject jrect = jsonObj.value("rect").toObject();
            rect.setX((int)jrect.value("x").toDouble());
            rect.setY((int)jrect.value("y").toDouble());
            rect.setWidth((int)jrect.value("width").toDouble());
            rect.setHeight((int)jrect.value("height").toDouble());
        }
        bool capturePic = jsonObj.value("capturePic").toBool(false);
        bool canResize =  jsonObj.value("canResize").toBool(true);
        QString postPath = jsonObj.value("submitLink").toString();
        QString cancelLink = settings.value("Wizzard/serverDomain","http://192.168.4.142:8080").toString()
                .append(settings.value("Wizzard/startPath","/OtwGateway/wizard/device/registration/residence").toString());
        if(jsonObj.contains("cancelLink")){
            cancelLink = jsonObj.value("cancelLink").toString();
        }

        emit launchTvReg(postPath, cancelLink, capturePic, canResize, rect);
        return;
    }
}

void WWeb::openUrl(QString url)
{
    QUrl qurl = ui->webView->url();
    if(QUrl(url).topLevelDomain().isEmpty()){
        qurl.setPath(url);
    }else{
        qurl = QUrl(url);
    }

    qDebug() << "WWeb::openUrl "<< qurl;
    ui->webView->load(qurl);
}
