#include "rectselectscene.h"

RectSelectScene::RectSelectScene(QObject *parent):
    QGraphicsScene(parent)
{
    imageItem = 0;
    mode = Idle;

    background = new QGraphicsItemGroup();

    this->addItem(background);
    canResize = false;

    rectRegion = new QGraphicsRectItem(0,0,100,200);
    rectRegion->setBrush(QColor(255, 0, 0, 56));
    rectRegion->setPen(QColor(255, 0, 0, 0));

    rectResize = new QGraphicsRectItem(94.5,194.5,5,5,rectRegion);
    rectResize->setBrush(QColor(0, 255, 0, 56));
    rectResize->setPen(QPen(QColor(255,0,0,56),1,Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin));
    //rectResize->setFlag(QGraphicsItem::ItemIsMovable,true);

    this->addItem(rectRegion);
}

void RectSelectScene::setImage(QByteArray image)
{
    if(image.isEmpty()) return;
    if(imageItem != 0){
        this->removeItem(imageItem);
        delete imageItem;
    }
    QImage img = QImage::fromData(image,"jpg");
    imageItem = new QGraphicsPixmapItem(QPixmap::fromImage(img),background);
}

void RectSelectScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if(imageItem == 0 || !canResize) return;
    if (mouseEvent->button() != Qt::LeftButton)
        return;
    QGraphicsItem * item = itemAt(mouseEvent->scenePos(),QTransform());

    if( item == rectResize){
        mode = ResizeRect;
        lastPos = mouseEvent->scenePos();
        startRect = getRect();
    }
    else if
            (item == rectRegion){
        mode = MoveRect;
        lastPos = mouseEvent->scenePos();
        startRect = getRect();
    }
    else
    {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
    }
}

void RectSelectScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if(imageItem == 0 || !canResize) return;
    if( mode == ResizeRect)
    {
        resizeRect(mouseEvent->scenePos());
    }
    else if( mode == MoveRect)
    {
        moveRect(mouseEvent->scenePos());
    }
    else
    {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
    }
}

void RectSelectScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if(imageItem == 0 || !canResize) return;
    if( mode == ResizeRect){
        resizeRect(mouseEvent->scenePos());
        mode = Idle;
    }
    else if( mode == MoveRect)
    {
        moveRect(mouseEvent->scenePos());
        mode = Idle;
    }
    else
    {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
    }
}


void RectSelectScene::resizeRect(QPointF mouse)
{
    QPointF diff = mouse - lastPos;
    QRectF rectRegionR = getRect();
    rectRegionR.setX(round(startRect.x()));
    rectRegionR.setY(round(startRect.y()));
    rectRegionR.setWidth(round(startRect.width() + diff.x()));
    rectRegionR.setHeight(round(startRect.height() + diff.y()));
    setRect(rectRegionR);
}

void RectSelectScene::moveRect(QPointF mouse)
{
    QPointF diff = mouse - lastPos;
    QRectF rectRegionR = getRect();
    rectRegionR.setX(round(startRect.x() + diff.x()));
    rectRegionR.setY(round(startRect.y() + diff.y()));
    rectRegionR.setWidth(startRect.width());
    rectRegionR.setHeight(startRect.height());
    setRect(rectRegionR);
}

void RectSelectScene::setRect(QRectF rect, bool force)
{
    if(!force){
        if(imageItem == 0) return;
        if(rect.x()+rect.width() > imageItem->boundingRect().width()){
            if(mode == MoveRect){
                int w = rect.width();
                rect.setX(imageItem->boundingRect().width()-rect.width());
                rect.setWidth(w);
            }else{
                rect.setWidth(imageItem->boundingRect().width()-rect.x());
            }
        }

        if(rect.y()+rect.height() > imageItem->boundingRect().height()){
            if(mode == MoveRect){
                int h = rect.height();
                rect.setY(imageItem->boundingRect().height()-rect.height());
                rect.setHeight(h);
            }else{
                rect.setHeight(imageItem->boundingRect().height()-rect.y());
            }
        }

        if(rect.x() < 0){
            int w = rect.width();
            rect.setX(0);
            rect.setWidth(w);
        }
        if(rect.y() < 0){
            int h = rect.height();
            rect.setY(0);
            rect.setHeight(h);
        }

        if(rect.width() < 8) rect.setWidth(8);
        if(rect.height() < 8) rect.setHeight(8);
    }
    rectRegion->setRect(rect);
    QRectF rectResizeR = QRectF(rect.x()+rect.width()-5.5,rect.y()+rect.height()-5.5,5,5);
    rectResize->setRect(rectResizeR);
}

