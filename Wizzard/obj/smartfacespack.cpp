#include "smartfacespack.h"



SmartFacesPack::SmartFacesPack(QNetworkAccessManager * manager, QString postPath, QString clientId, std::vector<QByteArray * > faces, QObject *parent) :
    QObject(parent)
{
    aborted = false;
    this->postPath = postPath;
    reply = 0;
    multiPart = 0;
    this->manager = manager;
    attempts = 0;
    this->faces = faces;
    this->clientId = clientId;
    isSending = false;
}

SmartFacesPack::~SmartFacesPack()
{
    qDebug() << "Deleteing SmartFacesPack data";
    for(std::vector<QByteArray *>::iterator f = faces.begin(); f != faces.end(); ++f){
        delete (*f);
    }
}

void SmartFacesPack::send()
{
    if(isSending) return;
    isSending = true;

    makeRequest();
}

void SmartFacesPack::makeRequest()
{
    //QUrl url(settings.value("SmartFacesPack/url","http://192.168.4.142:8080/OtwGateway/admin/client/images/save").toString());
    QUrl url(settings.value("Wizzard/serverDomain","http://192.168.4.142:8080").toString().append(postPath));
    qDebug() << "Posting faces to: " << url.toString();
    QNetworkRequest request(url);
    request.setRawHeader("User-Agent",settings.value("userAgent","smartAgent").toString().toStdString().c_str());

    QHttpMultiPart * mp  = getMultipart();
    reply = manager->post(request, mp);
    reply->setParent(this);

    connect(reply, SIGNAL(finished()), this, SLOT(nrFinished()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(nrError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(nrSslErrors(QList<QSslError>)));
}

QHttpMultiPart * SmartFacesPack::getMultipart()
{
    if(multiPart != 0){
        delete multiPart;
    }

    QHttpPart textPart;
    QHttpPart imagePart;

    multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    multiPart->setParent(this);

    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"clientId\""));
    textPart.setBody(clientId.toUtf8());
    multiPart->append(textPart);

    int i = -1;
    for(std::vector<QByteArray *>::iterator f = faces.begin(); f != faces.end(); ++f){
        i++;
        /// tv.full image data JPG
        imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
        imagePart.setHeader(QNetworkRequest::ContentDispositionHeader,
                            QVariant(QString("form-data; name=\"files[%1]\"; filename=\"image.jpg\"").arg(i)));
        imagePart.setBody(*(*f));
        multiPart->append(imagePart);
    }
    return multiPart;
}

void SmartFacesPack::nrError(QNetworkReply::NetworkError code __attribute__ ((unused)))
{
    qDebug() << "Network error: " << reply->errorString();
}

void SmartFacesPack::nrSslErrors(const QList<QSslError> & errors __attribute__ ((unused)))
{
    qDebug() << "SSL ERROR";
    reply->ignoreSslErrors();
}

void SmartFacesPack::nrFinished()
{
    if(!aborted)
    {
        if(reply->error() != QNetworkReply::NoError){
            if(++attempts > settings.value("SmartFacesPack/maxAttemts",3).toInt()){
                qDebug() << "Network request FAIL, maxAttempts exeeded DROPING DATA";
                abort();
            }else{
                qDebug() << "Network request FAIL retrying";
                QTimer::singleShot(settings.value("SmartFacesPack/retryAfterMs",5000).toInt(),this,SLOT(makeRequest()));
            }
        }else{
            qDebug() << "Network request successful";
            done();
        }
    }
}

void SmartFacesPack::abort()
{
    static const QMetaMethod failedSignal = QMetaMethod::fromSignal(&SmartFacesPack::failed);
    if (this->isSignalConnected(failedSignal)) {
        emit failed(this);
    }else{
        deleteLater();
    }
}


void SmartFacesPack::done()
{
    QByteArray bytes = reply->readAll();
    QJsonDocument doc = QJsonDocument::fromJson(bytes);
    qDebug() << "Supplied redirect link: "<< bytes;

    QJsonObject jsonObj = doc.object();
    redirectLink = jsonObj.value("redirectLink").toString();

    static const QMetaMethod finishedSignal = QMetaMethod::fromSignal(&SmartFacesPack::finished);
    if (this->isSignalConnected(finishedSignal)) {
        emit finished(this);
    }else{
        deleteLater();
    }
}

QString SmartFacesPack::getRedirectLink()
{
    if(redirectLink.count() == 0) return settings.value("Wizzard/serverDomain","http://192.168.4.142:8080").toString()
            .append(settings.value("Wizzard/startPath","/OtwGateway/wizard/device/registration/residence").toString());
    return redirectLink;
}

void SmartFacesPack::stop()
{
    aborted = true;
    if(reply != 0 && !reply->isFinished()) reply->abort();
}
