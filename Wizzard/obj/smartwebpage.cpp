#include "smartwebpage.h"

SmartWebPage::SmartWebPage(QObject *parent):
    QWebPage(parent)
{


}

QString SmartWebPage::userAgentForUrl ( const QUrl & url ) const
{
    return userAgent();
}

const char * SmartWebPage::userAgent () const
{
    return settings.value("userAgent","smartAgent").toString().toStdString().c_str();
}
