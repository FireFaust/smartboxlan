#ifndef SMARTFACESPACK_H
#define SMARTFACESPACK_H

#include <vector>

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDebug>
#include <QHttpMultiPart>
#include <QTimer>
#include <QSettings>
#include <QMetaMethod>

#include <QJsonValue>
#include <QJsonObject>
#include <QJsonDocument>

class SmartFacesPack : public QObject
{
    Q_OBJECT
public:
    explicit SmartFacesPack(QNetworkAccessManager * manager, QString postPath, QString clientId, std::vector<QByteArray * > faces, QObject *parent = 0);
    ~SmartFacesPack();
    void send();
    QString getRedirectLink();
    void stop();
    bool aborted;

signals:
    void finished(SmartFacesPack *);
    void failed(SmartFacesPack *);
    void progress(int,int);

public slots:

private slots:
    void nrError(QNetworkReply::NetworkError code);
    void nrFinished();
    void nrSslErrors(const QList<QSslError> & errors);
    void makeRequest();

private:

    QNetworkAccessManager * manager;
    QNetworkReply * reply;
    QHttpMultiPart * multiPart;
    std::vector<QByteArray * > faces;
    QString clientId;
    QSettings settings;
    QString postPath;
    QString redirectLink;

    void done();
    void abort();
    QHttpMultiPart * getMultipart();
    int attempts;
    bool isSending;

};

#endif // SMARTFACESPACK_H
