#ifndef SMARTTVRECPACK_H
#define SMARTTVRECPACK_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDebug>
#include <QHttpMultiPart>
#include <QTimer>
#include <QSettings>
#include <QMetaMethod>
#include <QRect>

#include <QJsonValue>
#include <QJsonObject>
#include <QJsonDocument>

class SmartTvRecPack : public QObject
{
    Q_OBJECT
public:
    explicit SmartTvRecPack(QNetworkAccessManager * manager, QString postPath, std::vector<QByteArray> tvRectPic, QRectF tvRect, QPointF picRect, QObject *parent = 0);
    ~SmartTvRecPack();
    void send();
    QString getRedirectLink();
    void stop();

signals:
    void finished(SmartTvRecPack *);
    void failed(SmartTvRecPack *);
    void progress(int,int);

public slots:

private slots:
    void nrError(QNetworkReply::NetworkError code);
    void nrFinished();
    void nrSslErrors(const QList<QSslError> & errors);
    void makeRequest();

private:

    QNetworkAccessManager * manager;
    QNetworkReply * reply;
    QHttpMultiPart * multiPart;

    std::vector<QByteArray> tvRectPic;
    QRectF tvRect;
    QPointF picRect;

    QSettings settings;
    QString postPath;
    QString redirectLink;

    void done();
    void abort();
    QHttpMultiPart * getMultipart();
    int attempts;
    bool isSending;
    bool aborted;
};

#endif // SMARTTVRECPACK_H
