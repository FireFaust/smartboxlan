#ifndef SMARTWEBPAGE_H
#define SMARTWEBPAGE_H

#include <QWebPage>
#include <QSettings>

class SmartWebPage : public QWebPage
{
    Q_OBJECT
public:
    SmartWebPage(QObject * parent = 0);
    QString userAgentForUrl ( const QUrl & url ) const;
    const char * userAgent () const;

    QSettings settings;
};

#endif // SMARTWEBPAGE_H
