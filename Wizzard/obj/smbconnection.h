#ifndef SMBCONNECTION_H
#define SMBCONNECTION_H

#include <QString>

class SmbConnection{
public:
    SmbConnection();
    SmbConnection(const SmbConnection &other);

    inline void setIpPort(QString ipPort){this->ipPort = ipPort;}
    inline QString getIpPort(){return ipPort;}

private:
    QString ipPort;
};

#endif // SMBCONNECTION_H
