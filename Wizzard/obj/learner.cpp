#include "learner.h"

Learner::Learner(QObject *parent) :
    QObject(parent)
{
}


void Learner::terminate()
{
    qDebug() << "Learner::terminate";
    emit finished();
}

void Learner::addFace(cv::Mat *face)
{
     double precision = learn.addFace(face);
     emit faceAdded(precision,learn.getTrainedFaces());
}

std::vector<QByteArray *> Learner::getGoodFaces()
{
    return learn.getFaces();
}


void Learner::saveToFile(QString filename)
{
    learn.saveToFile(filename);
}

void Learner::clear()
{
    learn.clear();
}

int Learner::facesTrained()
{
    return learn.getTrainedFaces();
}
