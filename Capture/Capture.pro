#-------------------------------------------------
#
# Project created by QtCreator 2013-10-31T12:11:45
#
#-------------------------------------------------

QT       += core network serialport

QT       -= gui

TARGET = Capture
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    framegrabber.cpp \
    frameprocessor.cpp \
    framesender.cpp \
    commandandcontrol.cpp \
    arduinointerface.cpp \
    picparser.cpp \
    dispatcher.cpp \
    smartservice.cpp

HEADERS += \
    framegrabber.h \
    frameprocessor.h \
    framesender.h \
    commandandcontrol.h \
    arduinointerface.h \
    picparser.h \
    dispatcher.h \
    smartservice.h

INCLUDEPATH += ../PicParse
LIBS     += -L../PicParse -lPicParse

INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core


include(http/http.pri)
include(obj/obj.pri)
include(stat/stat.pri)

RESOURCES += \
    res.qrc

OTHER_FILES += \
    res/www/index.html \
    res/www/favicon.ico \
    res/www/style.css

