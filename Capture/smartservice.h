#ifndef SMARTSERVICE_H
#define SMARTSERVICE_H

#include <QObject>

#include "statevent.h"

class SmartService: public QObject
{
    Q_OBJECT
public:
    SmartService(QObject *parent = 0);

signals:
    void hadEvent(StatEvent event);
    void finished();

public slots:
    virtual void doTerminate(bool force = true) = 0;
};

#endif // SMARTSERVICE_H
