INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += $$PWD/tcpserver.h \
    tcpserver/tcpserver.h \
    tcpserver/tcprequesthandler.h \
    tcpserver/tcpconnectionpool.h \
    tcpserver/tcpconnection.h \
    tcpserver/tcprequest.h

SOURCES += $$PWD/tcpserver.cpp \
    tcpserver/tcprequesthandler.cpp \
    tcpserver/tcpconnectionpool.cpp \
    tcpserver/tcpconnection.cpp \
    tcpserver/tcprequest.cpp
