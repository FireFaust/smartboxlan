#ifndef ARDUINOINTERFACE_H
#define ARDUINOINTERFACE_H

#include <QObject>
#include <QSerialPort>
#include <QString>
#include <QSerialPortInfo>
#include <QDebug>
#include <QByteArray>
#include <QTimer>
#include <QMutexLocker>
#include <QMutex>

enum ON_STAT{
    TV_OFF = 0,
    TV_ON = 1,
    TV_UNKNOWN = 2
};

class ArduinoInterface : public QObject
{
    Q_OBJECT
public:
    explicit ArduinoInterface(QObject *parent = 0);
    ~ArduinoInterface();
signals:
    void IrEvent();
    void OnEvent();
    void OffEvent();

public slots:
    void sendPing();
    ON_STAT isOn();

private slots:
    void grabFrame();
    void readData();
    void handshakeTimedout();
    void doHandshake();
    void reconnect();
    void onSerialError(QSerialPort::SerialPortError error);

private:
    bool hasDevice;
    bool hasHandshake;
    bool reconnectPending;

    bool findDevice();
    bool rescanDevices();
    bool connectDevice();
    void tryConnect();

    int handshakeInitTimeout;
    int handshakeTimeout;
    int reconnectTimeout;
    int pingTimeout;

    QString portNamePrefix;
    QSerialPort * port;
    QTimer pingTimer;

    QList<QSerialPortInfo> portsAvailable;
    QList<QSerialPortInfo>::Iterator currentPortIterator;

    void processLine(QString line);

    mutable QMutex mutex;
};

#endif // ARDUINOINTERFACE_H
