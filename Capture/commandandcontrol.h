#ifndef COMMANDANDCONTROL_H
#define COMMANDANDCONTROL_H

#define BILLION 1E6

#include <ctime>
#include <QObject>
#include <QResource>
#include <QFile>
#include <QThread>
#include <QJsonDocument>
#include <QJsonObject>
#include <QUdpSocket>
#include <QNetworkInterface>
#include <QMutex>
#include <QMutexLocker>
#include <QUrl>

#include "httplistener.h"
#include "httprequesthandler.h"

#include "framegrabber.h"
#include "frameprocessor.h"
#include "framesender.h"

#include "statevent.h"
#include "stateventqueue.h"
#include "smartservice.h"
#include "obj/smartfiledownloader.h"

#include "obj/smartlog.h"
#include "obj/smarthistory.h"

class CommandAndControl : public HttpRequestHandler
{
    Q_OBJECT
public:
    explicit CommandAndControl(QObject *parent = 0);
    ~CommandAndControl();

    FrameGrabber * frameGrabber;
    FrameProcessor * frameProcessor;
    FrameSender * frameSender;
    QObject * dispacher;

signals:

public slots:
    void addEvent(StatEvent event);
    void doTerminate(bool force = true);
    void load(SmartSettings settings);

private:
    mutable QMutex mutex;

    QNetworkConfigurationManager confMgr;
    HttpListener * listener;
    QUdpSocket * udpSocket;
    QSettings webSettings;
    QHostAddress udpGroupAddress;
    quint64 port;
    QTimer * udpRebindTimer;
    SmartSettings settings;
    StatEventQueue events;
    QSettings globSettings;

    SmartFileDownloader * regionOptDownloader;
    QTimer * regionOptTimer;

    void reload();

    void service(HttpRequest& request, HttpResponse& response);
    // responses
    void httpScanid(HttpRequest& request, HttpResponse& response);
    void httpDefault(HttpRequest& request, HttpResponse& response);
    void httpStat(HttpRequest& request, HttpResponse& response);
    void httpGetFrame(HttpRequest& request, HttpResponse& response);
    void httpSettings(HttpRequest& request, HttpResponse& response);
    void httpGetFace(HttpRequest& request, HttpResponse& response);

private slots:
    void processUdpDatagrams();
    void bindUdp();

    void requestRegionOpt();
    void requestRegionFinished(SmartFileDownloader * dl);
    void requestRegionFailed(SmartFileDownloader * dl);

    void cUrl();
};

#endif // COMMANDANDCONTROL_H
