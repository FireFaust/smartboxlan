#ifndef FRAMESENDER_H
#define FRAMESENDER_H


#include <QTimer>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QUrl>
#include <QHttpMultiPart>
#include <QFile>
#include <deque>

#include "smartframe.h"
#include "picparse.h"
#include "smartservice.h"
#include "settings/smartsettings.h"
#include "obj/smartframepack.h"

class FrameSender : public SmartService
{
    Q_OBJECT
public:
    explicit FrameSender(QObject *parent = 0);
    ~FrameSender();

signals:

    void finished();

public slots:
    void doTerminate(bool force = true);
    void SendFrame(SmartFrame * frame);
    void load(SmartSettings settings);

private slots:
    void packageSent(SmartFramePack * package);
    void packageError(SmartFramePack * package);

private:

    std::deque<SmartFrame *> frames;
    QNetworkAccessManager * manager;
    SmartSettings settings;
    //std::deque<SmartFramePack *> packages;
    int requestCount;

    void SendFrames();
    void stopAndRelaod();

    bool terminateRequested;

    void clearQueue();
};

#endif // FRAMESENDER_H
