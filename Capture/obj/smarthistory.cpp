#include "smarthistory.h"

SmartHistory* SmartHistory::smartHist = 0;
SmartHistory* SmartHistory::instance()
{
    static QMutex mutex;
    if (!smartHist)
    {
        mutex.lock();

        if (!smartHist)
            smartHist = new SmartHistory();

        mutex.unlock();
    }
    return smartHist;
}

SmartHistory * SmartHistory::operator -> (){
    static QMutex mutex;
    if (!smartHist)
    {
        mutex.lock();

        if (!smartHist)
            smartHist = new SmartHistory();

        mutex.unlock();
    }

    return smartHist;
}

void SmartHistory::drop()
{
    static QMutex mutex;
    mutex.lock();
    delete smartHist;
    smartHist = 0;
    mutex.unlock();
}

SmartHistory::SmartHistory()
{
    maxFaces = 50;
    faceId = 0;
}

void SmartHistory::addFaces(std::vector<QByteArray *> faces)
{
    //static QMutex mutex;
    //mutex.lock();

    if(faces.empty()) return;
    SmartHistory * hist = SmartHistory::instance();
    for(std::vector<QByteArray *>::iterator f = faces.begin(); f != faces.end(); ++f)
    {
        hist->addFace( QByteArray(*(*f)));
    }
    //mutex.unlock();
}

void SmartHistory::addFace(QByteArray face)
{
    faceId++;

    if(faceId > maxFaces && faceId > 30000) faceId = 0;
    lastFaces.push_front(face);
    lastFacesIds.push_front(faceId);
    if(lastFaces.size() > maxFaces) lastFaces.pop_back();
    if(lastFacesIds.size() > maxFaces) lastFacesIds.pop_back();
}

QByteArray SmartHistory::getFace(qint64 id)
{
    QMutexLocker locker(&mutex);
    QByteArray result;
    if(lastFaces.empty()) return result;
    if(id == -1){
        result = *(lastFaces.begin());
    }else{
        for(unsigned int i = 0; i < lastFacesIds.size(); i++)
        {
            if(lastFacesIds[i] == id){
                if(lastFaces.size()-1 < i) return result;
                result = lastFaces[i];
                break;
            }
        }
    }
    return result;
}


QJsonValue SmartHistory::getLastFaces()
{
    QMutexLocker locker(&mutex);
    QJsonArray result;
    for( std::deque<qint64>::iterator i = lastFacesIds.begin(); i != lastFacesIds.end(); ++i)
    {
        result.append(QString::number(*i));
    }
    return result;
}

