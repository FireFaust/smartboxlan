#ifndef SMARTHISTORY_H
#define SMARTHISTORY_H

#include <QObject>
#include <QMutex>
#include <QMutexLocker>
#include <QByteArray>
#include <QDateTime>
#include <QDebug>
#include <QJsonValue>
#include <QJsonArray>

#include <deque>
#include <vector>

class SmartHistory : public QObject
{
    Q_OBJECT
public:
    static SmartHistory * instance();
    SmartHistory * operator -> ();

    static void addFaces(std::vector<QByteArray *> faces);
    static void drop();
    QByteArray getFace(qint64 id = -1);
    QJsonValue getLastFaces();

signals:
    void newLog(QString msg, QString type);
private:
    SmartHistory();
    SmartHistory(const SmartHistory &); // hide copy constructor
    SmartHistory& operator=(const SmartHistory &); // hide assign op
    static SmartHistory* smartHist;

    std::deque<QByteArray> lastFaces;
    std::deque<qint64> lastFacesIds;
    void addFace(QByteArray face);
    qint64 faceId;
    qint64 maxFaces;

    mutable QMutex mutex;
};

#endif // SMARTHISTORY_H
