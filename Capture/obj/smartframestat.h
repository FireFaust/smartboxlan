#ifndef SMARTFRAMESTAT_H
#define SMARTFRAMESTAT_H

#include <QDateTime>
#include <QDebug>

class SmartFrameStat
{
public:
    SmartFrameStat();

    bool isProcessed;
    bool isSent;
    bool hasTv;
    bool hasWc;
    bool isSaved;
    qint64 wcCaptured;
    qint64 tvCaptured;

    qint64 procStart;
    qint64 procEnd;
    qint64 grabStart;
    qint64 grabEnd;

    qint64 frameSent;
    int send_attempts;
    int threadCount;
    double fpm;
};

#endif // SMARTFRAMESTAT_H
