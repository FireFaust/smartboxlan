#ifndef SMARTLOG_H
#define SMARTLOG_H

#include <QObject>
#include <QMutex>
#include <QDebug>
#include <QFile>
#include <QDir>
#include <QDateTime>
#include <deque>
#include <QJsonValue>
#include <QJsonArray>


struct LogMsg{
    qint64 timestamp;
    QString type;
    QString msg;
};

class SmartLog : public QObject
{
    Q_OBJECT
public:
    static SmartLog * instance();
    SmartLog * operator -> ();

    static void log(QString msg, QString type, qint64 timestamp);
    static void log(QString msg, QString type);
    static void log(QString msg);
    QJsonValue getLastLogs();
    inline qint64 getStarted(){return started;}

    static void drop();

signals:
    void newLog(QString msg, QString type, qint64 timestamp);
private:
    SmartLog();
    SmartLog(const SmartLog &); // hide copy constructor
    SmartLog& operator=(const SmartLog &); // hide assign op
    static SmartLog* smartLog;

    QString logPath;
    QDir logDir;
    qint64 started;

    void writeLog(QString log);
    void addLastLogs(QString line);
    std::deque<QString> lastLogs;

    mutable QMutex mutex;
};

#endif // SMARTLOG_H
