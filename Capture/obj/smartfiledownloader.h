#ifndef SMARTFILEDOWNLOADER_H
#define SMARTFILEDOWNLOADER_H

#include <vector>

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDebug>
#include <QHttpMultiPart>
#include <QTimer>
#include <QSettings>
#include <QMetaMethod>
#include <QMap>

#include <QJsonValue>
#include <QJsonObject>
#include <QJsonDocument>

class SmartPostValue
{
public:
    enum SmartPostValueType{None,Str,Byt};
    inline SmartPostValue(){type = None;}
    inline SmartPostValue(const QString value){type = Str;sValue = value;}
    inline SmartPostValue(const QByteArray value){type = Byt; bValue = value; bType = "jpg";}
    inline SmartPostValue(QByteArray value, QString type){this->type = Byt; bValue = value; bType = type;}
    inline explicit SmartPostValue(const SmartPostValue& other){type = other.type; sValue = other.sValue; bValue = other.bValue; bType = other.bType;}
    inline SmartPostValueType valueType(){return type;}
    inline bool isEmpty() {return type==None?true:(type==Str?sValue.isEmpty():bValue.isEmpty());}
    inline bool isString() {return type == Str;}
    inline bool isByteArray() {return type == Byt;}
    inline QString toString() {return sValue;}
    inline QByteArray toByteArray() {if(type==Str) return sValue.toUtf8(); return bValue;}
    inline QString fileType() {return bType;}
private:
    SmartPostValueType type;
    QString sValue;
    QByteArray bValue;
    QString bType;
};

class SmartFileDownloader : public QObject
{
    Q_OBJECT
public:
    explicit SmartFileDownloader(QString url, QObject *parent = 0, QNetworkAccessManager * manager = 0);
    explicit SmartFileDownloader(QString url, QMap<QString,SmartPostValue> values, QObject *parent = 0, QNetworkAccessManager * manager = 0);
    explicit SmartFileDownloader(QString url, QMap<QString,SmartPostValue> values, QMap<QString,QString> headers, QObject *parent = 0, QNetworkAccessManager * manager = 0);
    ~SmartFileDownloader();
    void send();
    void stop();
    inline QByteArray getResult(){return result;}

signals:
    void finished(SmartFileDownloader *);
    void failed(SmartFileDownloader *);
    void progress(int,int);

public slots:

private slots:
    void nrError(QNetworkReply::NetworkError code);
    void nrFinished();
    void nrSslErrors(const QList<QSslError> & errors);
    void makeRequest();

private:
    void constructor(QString url, QMap<QString,SmartPostValue> values, QMap<QString,QString> headers, QNetworkAccessManager * manager = 0);

    QNetworkAccessManager * manager;
    QNetworkReply * reply;
    QMap<QString,SmartPostValue> values;
    QMap<QString,QString> headers;
    QString url;

    void done();
    void abort();
    QHttpMultiPart * getMultipart();
    int attempts;
    bool isSending;
    bool aborted;
    int maxAttemts;

    QByteArray result;
};

#endif // SMARTFILEDOWNLOADER_H
