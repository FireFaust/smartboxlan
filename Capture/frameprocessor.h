#ifndef FRAMEPROCESSOR_H
#define FRAMEPROCESSOR_H

#include <vector>
#include <deque>

#include <QThread>
#include <QDebug>
#include <QDateTime>
#include <QMutex>
#include <QMutexLocker>

#include "smartframe.h"
#include "picparser.h"
#include "settings/smartsettings.h"
#include "smartservice.h"

class FrameProcessor : public SmartService
{
    Q_OBJECT
public:
    explicit FrameProcessor(QObject *parent = 0);

signals:
    void frameReady(SmartFrame * frame);
    void finished();

public slots:
    void doTerminate(bool force = true);
    void queueFrame(SmartFrame * frame);
    void frameProcessed(SmartFrame * frame, int processId);
    void load(SmartSettings settings);
    float fpm();

private:
    std::vector<QThread *> parserThreads;
    std::vector<PicParser *> parsers;
    std::vector<bool> parserBusy;

    std::deque<SmartFrame *> frames;
    SmartSettings settings;

    void initPicParsers();
    void stopAndRelaod();
    void checkQueue();

    std::deque<quint64> fpmFrames;
    quint64 fpmMSecons;
    quint64 started;
    float fpmCurrent;

    bool closeWhenDone;
    bool mustReaload;
    void  fpmNewFrame();

    mutable QMutex mutex;
};

#endif // FRAMEPROCESSOR_H
