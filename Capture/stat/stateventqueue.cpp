#include "stateventqueue.h"

StatEventQueue::StatEventQueue()
{
    maxEvents = 1000;
    eventsExpireAfter = 1000*60*60*24*3;
}

void StatEventQueue::addEvent(StatEvent event)
{
    if(!queue.empty() && queue[queue.size()-1].time > event.time){
        qWarning() << "Cann't log stat event (time goes backwards)!";
        return;
    }
    queue.push_back(event);
    quint64 expireTime = QDateTime::currentDateTime().toMSecsSinceEpoch() - eventsExpireAfter;
    while(queue.size() > maxEvents || (queue.size() > 0 && queue[0].time < expireTime)){
        queue.pop_front();
    }
}

QJsonValue StatEventQueue::getJson(){
    return getJson(queue.begin(), queue.end());
}

QJsonValue StatEventQueue::getJson(std::deque<StatEvent>::iterator begin, std::deque<StatEvent>::iterator end)
{
    QJsonArray result;
    for(std::deque<StatEvent>::iterator i = begin; i < end; i++)
    {
        QJsonObject statEvent;
        statEvent.insert("type",(*i).type);
        statEvent.insert("message",(*i).message);
        statEvent.insert("time",QJsonValue((double)(*i).time));
        result.append(statEvent);
    }
    return result;
}


