#ifndef STATEVENT_H
#define STATEVENT_H

#include <QString>
#include <QDateTime>

class StatEvent
{
public:
    StatEvent();
    StatEvent(QString type, QString message);
    StatEvent(QString type, QString message, quint64 time);

    QString type;
    QString message;
    quint64 time;

private:
    void init(QString type = "unknown", QString message = "", quint64 time = QDateTime::currentDateTime().toMSecsSinceEpoch());
};

#endif // STATEVENT_H
