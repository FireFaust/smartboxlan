#include "arduinointerface.h"

ArduinoInterface::ArduinoInterface(QObject *parent) :
    QObject(parent)
{
    hasDevice = false;
    hasHandshake = false;
    portNamePrefix = "ttyACM";
    reconnectPending = false;
    port = 0;

    handshakeInitTimeout = 3000;
    handshakeTimeout = 5000;
    reconnectTimeout = 2000;
    pingTimeout = 5*60*1000;

    connect(&pingTimer, SIGNAL(timeout()), this, SLOT(sendPing()));
    tryConnect();
}

ArduinoInterface::~ArduinoInterface()
{
    //port->close();
}

void ArduinoInterface::reconnect()
{
    if(pingTimer.isActive()) pingTimer.stop();
    reconnectPending = false;
    hasHandshake = false;
    if(portsAvailable.size() == 0 || portsAvailable.end() == currentPortIterator){
        if(!rescanDevices()){
            qDebug() << "No devices found";
            // Retry after timeout
            tryConnect();
            return;
        }
    }
    if(!connectDevice()){
        // Retry after timeout
        tryConnect();
        return;
    }
}

void ArduinoInterface::tryConnect(){
    hasDevice = false;
    if(reconnectPending) return;
    reconnectPending = true;
    QTimer::singleShot(reconnectTimeout,this, SLOT(reconnect()));
};

bool ArduinoInterface::rescanDevices()
{
    portsAvailable.clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        if(!info.portName().startsWith(portNamePrefix)){
            continue;
        }
        portsAvailable.push_back(info);
    }
    if(portsAvailable.size() == 0) return false;
    currentPortIterator = portsAvailable.begin();
    return true;
}

bool ArduinoInterface::connectDevice()
{
    QSerialPortInfo info = *currentPortIterator;
    qDebug() << "Test Device: " << info.portName();

    if(port != 0){
        if(port->isOpen()) port->close();
        delete port;
    }
    port = new QSerialPort; //create the port
    connect(port, SIGNAL(readyRead()), this, SLOT(readData()));
    connect(port, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(onSerialError(QSerialPort::SerialPortError)));

    port->setPortName(info.portName());
    if(!port->open(QIODevice::ReadWrite)){
        delete port;
        return false;
    }; //open the port


    port->setBaudRate(QSerialPort::Baud9600);
    port->setDataBits(QSerialPort::Data8);
    port->setParity(QSerialPort::NoParity);
    port->setStopBits(QSerialPort::OneStop);
    port->setFlowControl(QSerialPort::NoFlowControl);


    if(!port->isOpen())
    {
        qDebug() << "Failed to open serial port: " << info.portName() << " error: " << port->errorString() ;
        return false;
    }

    QTimer::singleShot(handshakeInitTimeout,this, SLOT(doHandshake()));
    return true;
}

void ArduinoInterface::doHandshake(){
    port->write("h");
    if (!port->waitForBytesWritten(3000)) {
        qDebug() << "Handshake failed (cant send)";
        tryConnect();
        return;
    }
     QTimer::singleShot(handshakeTimeout,this, SLOT(handshakeTimedout()));
}


void ArduinoInterface::handshakeTimedout()
{
    if(hasHandshake) return;
    qDebug() << "Handshake timed out. Try next device in " << reconnectTimeout << " msec.";
    tryConnect();
}


void ArduinoInterface::sendPing()
{
    port->write("p");
    if (!port->waitForBytesWritten(3000)) {
        qDebug() << "Ping failed (cant send)";
        tryConnect();
        return;
    }
    qDebug() << "Ping sent";
}

ON_STAT ArduinoInterface::isOn()
{
    QMutexLocker locker(&mutex);
    if(port == 0 || !port->isOpen()|| !port->isWritable()) return TV_UNKNOWN;
    port->readAll();
    port->blockSignals(true);
    port->write("s\n");
    if (!port->waitForBytesWritten(200)) {
        qDebug() << "isOn failed (cant send)";
        tryConnect();
        return TV_UNKNOWN;
    }

    while(!port->canReadLine()){
         qDebug() << "waitForReadyRead";
        if (!port->waitForReadyRead(2000)) {
            qDebug() << "isOn failed (no reply)";
            port->blockSignals(false);
            return TV_UNKNOWN;
        }
    }

    QString reply = port->readLine();
    port->blockSignals(false);

    if(reply.contains("1")){
        return TV_ON;
    }else if(reply.contains("0")){
        return TV_OFF;
    }else {
        return TV_UNKNOWN;
    }
}

void ArduinoInterface::grabFrame()
{
    QMutexLocker locker(&mutex);
    if(port == 0 || !port->isOpen()|| !port->isWritable()) return;
    port->write("f\n");
    if (!port->waitForBytesWritten(200)) {
        qDebug() << "grabFrame failed (cant send)";
        tryConnect();
    }
}

void ArduinoInterface::readData()
{
    if(!port->canReadLine()) return;
    while(port->canReadLine()){
        processLine(port->readLine());
    }
}

void ArduinoInterface::processLine(QString line)
{
    //qDebug() << "Incoming: " << line;
    if(!hasHandshake){
        if(!line.contains("handshake"))
        {
            qDebug() << "Wrong handshake";
            return;
        }else{
            qDebug() << "GOOD handshake";
            hasHandshake = true;

            if(isOn()==TV_OFF){
                emit OffEvent();
            }else{
                emit OnEvent();
            }
        }
        return;
    }

    if(line.contains("irevent"))
    {
        emit IrEvent();
    }
    else if(line.contains("turnoffevent"))
    {
        emit OffEvent();
    }
    else if(line.contains("turnonevent"))
    {
        emit OnEvent();
    }
    else if(line.contains("0"))
    {
        // Not event
    }
    else if(line.contains("1"))
    {
        // Not event
    }
    else
    {
        //qDebug() << "Wrong arduino data";
    }
}

void ArduinoInterface::onSerialError(QSerialPort::SerialPortError error)
{
    if(error != 0 && error != 13){
        emit OnEvent();
        currentPortIterator++;
        if(port->isOpen()) port->close();
        delete port;
        port = 0;
        tryConnect();
    }
}
