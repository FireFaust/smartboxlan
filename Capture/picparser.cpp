#include "picparser.h"

PicParser::PicParser(SmartSettings settings, int id, QObject *parent) :
    QObject(parent)
{
    this->id = id;
    this->settings = settings;
    parser = new PicParse(settings);
}

PicParser::~PicParser()
{
    delete parser;
}


void PicParser::ParsePic(SmartFrame * frame)
{
    //qDebug() << "From ParsePic thread: " << QThread::currentThreadId();
    //frame->saveToDisk(0);
    frame->process(parser);
    SmartHistory::addFaces(frame->getFaces());
    emit PicParsed(frame,id);
}
