#include "frameprocessor.h"

FrameProcessor::FrameProcessor(QObject *parent) :
    SmartService(parent)
{
    mustReaload = false;
    closeWhenDone = false;

    fpmCurrent = 0;
    fpmMSecons = 5*60*1000;
    started = QDateTime::currentDateTime().toMSecsSinceEpoch();

    initPicParsers();
}

void FrameProcessor::initPicParsers()
{
    qDebug() << "=== INIT " << settings.processor.parserCount << " parsers";
    PicParser * parser;
    QThread * thread;
    for(int i = 0; i < settings.processor.parserCount; i++){
        thread = new QThread();
        parser = new PicParser(settings,i,0);
        connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

        connect(parser, SIGNAL(PicParsed(SmartFrame *,int)), this, SLOT(frameProcessed(SmartFrame *,int)), Qt::QueuedConnection);
        parser->moveToThread(thread);
        parsers.push_back(parser);
        thread->start();

        parserThreads.push_back(thread);
        parserBusy.push_back(false);
    }
    started = QDateTime::currentDateTime().toMSecsSinceEpoch();
    fpmFrames.clear();
}

void FrameProcessor::load(SmartSettings settings)
{
    QMutexLocker locker(&mutex);
    bool settingsChanged = settings.processor != this->settings.processor;
    this->settings = settings;
    SmartFrame::sendFullTv = settings.processor.keepFullTv;
    SmartFrame::sendFullWc = settings.processor.keepFullWc;
    if(settingsChanged) stopAndRelaod();
}

void FrameProcessor::stopAndRelaod()
{
    mustReaload = true;
    bool busy = false;
    for(unsigned int i = 0; i < parserBusy.size(); i++){
        if(parserBusy[i]){
            busy = true;
            break;
        };
    }
    // Wait for all threads to quit;
    if(busy) return;

    for(unsigned int i = 0; i < parsers.size(); i++){
        delete parsers[i];
    }

    for(unsigned int i = 0; i < parserThreads.size(); i++){
        //parserThreads[i]->terminate();
        QMetaObject::invokeMethod(parserThreads[i], "quit", Qt::QueuedConnection);
        //delete parserThreads[i];
    }
    parserThreads.clear();
    parsers.clear();
    parserBusy.clear();
    // restart with new settings
    initPicParsers();
    mustReaload = false;
    checkQueue();
}


void FrameProcessor::queueFrame(SmartFrame * frame)
{
    if(frames.size() > 30) {
        //qDebug() << "Queue too large, discarding frame ";
        delete frame;
        return;
    }
    QMutexLocker locker(&mutex);
    //qDebug() << "From processor thread: " << QThread::currentThreadId()
    //         << " Queue size:" << frames.size();


    frames.push_back(frame);
    checkQueue();
}


void FrameProcessor::checkQueue()
{

    if(mustReaload){
        stopAndRelaod();
        return;
    }
    if(closeWhenDone && frames.size() == 0){
        bool busy = false;
        for(unsigned int i = 0; i < parserBusy.size(); i++){
            if(parserBusy[i]){
                busy = true;
                break;
            };
        }
        if(!busy){
            emit finished();
            return;
        }
    }
    for(unsigned int i = 0; i < parserBusy.size() && !frames.empty(); i++){
        if(parserBusy[i]) continue;
        parserBusy[i] = true;
        SmartFrame * frame  = frames.front();
        frame->stat.threadCount = settings.processor.parserCount;
        frames.pop_front();
        QMetaObject::invokeMethod(parsers[i], "ParsePic", Qt::QueuedConnection, Q_ARG( SmartFrame *, frame));
    }
}

void FrameProcessor::frameProcessed(SmartFrame * frame, int processId)
{

    QMutexLocker locker(&mutex);
    //qDebug() << "From frameProcessed thread: " << QThread::currentThreadId();
    parserBusy[processId] = false;

    if(!settings.processor.keepWhenNoFaces && frame->facesSize() == 0){
        delete frame;
    }else{
        emit frameReady(frame);
    }

    fpmNewFrame();
    if(closeWhenDone){
        qDebug() << "=== FPM: " << QString::number(fpmCurrent,'f',2) << " CLOSING ( " << frames.size() << " in queue)";
    }else{
        qDebug() << "=== FPM: " << QString::number(fpmCurrent,'f',2) << " BUFFER  ( " << frames.size() << " )";
    }
    checkQueue();
}

void FrameProcessor::doTerminate(bool force)
{

    QMutexLocker locker(&mutex);
    if(force){
        for(unsigned int i = 0; i < frames.size(); i ++){
            delete frames[i];
        }
        qDebug("FrameProcessor terminated by force!");
        emit finished();
        return;
    }
    closeWhenDone = true;
    checkQueue();
}

float FrameProcessor::fpm()
{
    QMutexLocker locker(&mutex);
    return fpmCurrent;
}


void FrameProcessor::fpmNewFrame()
{
    quint64 time = QDateTime::currentDateTime().toMSecsSinceEpoch();
    quint64 timeSinceStarted = time - started;
    fpmFrames.push_back(time);
    time -= fpmMSecons;
    while(!fpmFrames.empty() && *(fpmFrames.begin()) < time){
        fpmFrames.pop_front();
    }
    //qDebug() << timeSinceStarted << " < " << fpmMSecons << " | " << fpmFrames.size();
    if(timeSinceStarted < fpmMSecons){
        fpmCurrent = ((double)fpmFrames.size()/((double)timeSinceStarted/1000))*60;
    }else{
        fpmCurrent = ((double)fpmFrames.size()/((double)fpmMSecons/1000))*60;
    }
}


