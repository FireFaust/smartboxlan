#ifndef FRAMEGRABBER_H
#define FRAMEGRABBER_H

#include <QThread>
#include <QTimer>
#include <QDateTime>
#include <QString>
#include <QMutex>
#include <QMutexLocker>

#include "smartconv.h"
#include "picparse.h"
#include "smartcap.h"
#include "smartframe.h"
#include "settings/smartsettings.h"
#include "smartservice.h"

class FrameGrabber : public  SmartService
{
    Q_OBJECT
public:
    FrameGrabber(QObject *parent = 0);
    ~FrameGrabber();

    void cleanup();
signals:
    void frameGrabbed(SmartFrame * frame);
    void willGrabFrame();
    void finished();

public slots:
    void grabFramesRemote();
    void setGrabbing(bool isGrabbing);
    void setGrabbingOn();
    void setGrabbingOff();
    void doTerminate(bool force = true);
    void startTimerLoop();
    void load(SmartSettings settings);
    QByteArray getFrame(int source);
private slots:
    void grabFrames();
private:
    SmartCap *tv,*wc;
    QTimer *grab_timer;
    SmartSettings settings;
    int grabTimer;
    bool isGrabbing;

    void stopAll();
    void init();
    void captureAndProcess();
    mutable QMutex mutex;
};

#endif // FRAMEGRABBER_H
