#include "framegrabber.h"

FrameGrabber::FrameGrabber(QObject *parent):
    SmartService(parent)
{
    tv = wc = 0;
    isGrabbing = true;
    grab_timer = new QTimer();
    connect(grab_timer, SIGNAL(timeout()), this, SLOT(grabFrames()));
}

FrameGrabber::~FrameGrabber()
{
    if(tv != 0) delete tv;
    if(wc != 0) delete wc;
    delete grab_timer;
}

void FrameGrabber::load(SmartSettings settings)
{
    QMutexLocker locker(&mutex);
    // qDebug() << "FrameGrabber::load";
    bool settingsChanged = false;
    settingsChanged = settingsChanged || settings.tvSource != this->settings.tvSource;
    settingsChanged = settingsChanged || settings.wcSource != this->settings.wcSource;
    settingsChanged = settingsChanged || settings.grabTimer != this->settings.grabTimer;

    if(settingsChanged){
        stopAll();
        this->settings = settings;
        init();
    }else{
        this->settings = settings;
        if(tv == 0 || wc == 0) init();
    }
}

void FrameGrabber::setGrabbing(bool isGrabbing)
{
    this->isGrabbing = isGrabbing;
}

void FrameGrabber::setGrabbingOn()
{
    this->isGrabbing = true;
}

void FrameGrabber::setGrabbingOff()
{
    this->isGrabbing = false;
}

void FrameGrabber::stopAll()
{
    qDebug() << "FrameGrabber::stopAll";
    grab_timer->stop();
    if(tv != 0){
        delete tv;
        tv = 0;
    }
    if(wc != 0){
        delete wc;
        wc = 0;
    }
}

void FrameGrabber::init()
{
    // qDebug("FrameGrabber::init");
    grabTimer = settings.grabTimer;
    switch(settings.tvSource.sourceType){
    case OPENCV_CAP:
        tv = new SmartCap(OPENCV_CAP,settings.tvSource.sourceNum);
        break;
    case FLYCAP_CAP:
        tv = new SmartCap(FLYCAP_CAP,settings.tvSource.sourceNum);
        break;
    case VIDEO_CAP:
        tv = new SmartCap(settings.tvSource.filename.toStdString().c_str());
        break;
    case EASY_CAP:
        tv = new SmartCap(EASY_CAP,settings.tvSource.sourceNum);
        break;
    default:
        qWarning() << "Wrong settings for TV capture!";
    }

    switch(settings.wcSource.sourceType){
    case OPENCV_CAP:
        wc = new SmartCap(OPENCV_CAP,settings.wcSource.sourceNum);
        break;
    case FLYCAP_CAP:
        wc = new SmartCap(FLYCAP_CAP,settings.wcSource.sourceNum);
        break;
    case VIDEO_CAP:
        wc = new SmartCap(settings.wcSource.filename.toStdString().c_str());
        break;
    case EASY_CAP:
        wc = new SmartCap(EASY_CAP,settings.wcSource.sourceNum);
        break;
    default:
        qWarning() << "Wrong settings for WC capture!";
    }

}

void FrameGrabber::doTerminate(bool force __attribute__ ((unused)))
{
    QMutexLocker locker(&mutex);
    //  qDebug() << "From grabber thread doTerminate";
    stopAll();
    emit finished();
}

void FrameGrabber::startTimerLoop()
{
    QMutexLocker locker(&mutex);
    // qDebug("FrameGrabber::startTimerLoop");
    if(grab_timer->isActive()) return;
    grab_timer->start(grabTimer);
}

void FrameGrabber::grabFrames()
{
    QMutexLocker locker(&mutex);
    grab_timer->blockSignals(true);
    //qDebug("FrameGrabber::grabFrames");
    if(isGrabbing) captureAndProcess();
    grab_timer->blockSignals(false);
}

void FrameGrabber::grabFramesRemote()
{
    QMutexLocker locker(&mutex);
    grab_timer->blockSignals(true);
    if(isGrabbing) captureAndProcess();
    grab_timer->blockSignals(false);
}

void FrameGrabber::captureAndProcess()
{
    if(wc == 0 || tv == 0) return;
    //qDebug() << "From grabber thread grabFrames: " << QThread::currentThreadId();
    SmartFrame * frame = new SmartFrame();
    frame->stat.grabStart = QDateTime::currentDateTime().toMSecsSinceEpoch();

    emit willGrabFrame();
    if(!wc->prepareFrame()){
        qWarning("Failed to prepare wc frames");
        wc->init();
        delete frame;
        return;
    }
    frame->stat.tvCaptured = QDateTime::currentDateTime().toMSecsSinceEpoch();

    if(!tv->prepareFrame()){
        qWarning("Failed to prepare tv frames");
        tv->init();
        delete frame;
        return;
    }
    frame->stat.wcCaptured = QDateTime::currentDateTime().toMSecsSinceEpoch();

    if(!frame->setWc(wc->getFrame())){
        qWarning("Failed to wc get frames");
        wc->init();
        delete frame;
        return;
    }
    if(!frame->setTv(tv->getFrame())){
        qWarning("Failed to tv get frames");
        tv->init();
        delete frame;
        return;
    }

    frame->stat.grabEnd = QDateTime::currentDateTime().toMSecsSinceEpoch();

    emit hadEvent(StatEvent("info","frameGrabbed"));
    emit frameGrabbed(frame);

    // qDebug("FrameGrabber::frameGrabbed");
}

QByteArray FrameGrabber::getFrame(int source)
{
    QMutexLocker locker(&mutex);
    QByteArray result;

    if(wc == 0 || tv == 0) return result;
    if(source != 1) emit willGrabFrame();

    cv::Mat * frame;
    switch(source){
    case 1:
        if(tv == 0) return result;
        if(!tv->prepareFrame()){
            return result;
        }
        frame = tv->getFrame();
        break;
    default:
        if(wc == 0) return result;
        if(!wc->prepareFrame()){
            return result;
        }
        frame = wc->getFrame();
        break;
    }

    result = SmartConv::MatToJpg(*frame,87);
    delete frame;
    return result;
}
