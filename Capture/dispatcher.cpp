#include "dispatcher.h"

int Dispatcher::sigintpFd[2];
int Dispatcher::sigtermFd[2];

Dispatcher::Dispatcher(int &argc, char **argv) :
    QCoreApplication(argc, argv)
{
    // POSTIX SIGNALS ==========================================================
    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sigintpFd))
        qFatal("Couldn't create SIGINT socketpair");

    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sigtermFd))
        qFatal("Couldn't create TERM socketpair");
    snInt = new QSocketNotifier(sigintpFd[1], QSocketNotifier::Read, this);
    snTerm = new QSocketNotifier(sigtermFd[1], QSocketNotifier::Read, this);
    connect(snInt, SIGNAL(activated(int)), this, SLOT(handleSigInt()));
    connect(snTerm, SIGNAL(activated(int)), this, SLOT(handleSigTerm()));
    // END POSTIX SIGNALS ======================================================

    forceClose = false;
    closeAttempts = 0;
    doClose = false;

    qDebug()<<"From Commander thread grabFrames: "<<QThread::currentThreadId();

    qRegisterMetaType<SmartSettings>("SmartSettings");
    qRegisterMetaType<StatEvent>("StatEvent");
    qRegisterMetaType<SmartFrame>("SmartFrame");
    qRegisterMetaType<SmartFrame *>("SmartFrame *");

    settings.readFromFile("settings.json");
    frameProcessor.load(settings);
    frameGrabber.load(settings);
    frameSender.load(settings);
    controller.load(settings);

    controller.frameGrabber = &frameGrabber;
    controller.frameProcessor = &frameProcessor;
    controller.frameSender = &frameSender;
    controller.dispacher = this;

    QObject::connect(&frameGrabber, SIGNAL(frameGrabbed(SmartFrame *)), &frameProcessor, SLOT(queueFrame(SmartFrame *)), Qt::QueuedConnection);
    QObject::connect(&frameProcessor, SIGNAL(frameReady(SmartFrame *)), &frameSender, SLOT(SendFrame(SmartFrame *)), Qt::QueuedConnection);
    QObject::connect(&frameGrabber, SIGNAL(willGrabFrame()), &arduino, SLOT(grabFrame()), Qt::QueuedConnection);
    QObject::connect(&arduino, SIGNAL(OnEvent()), &frameGrabber, SLOT(setGrabbingOn()), Qt::QueuedConnection);
    QObject::connect(&arduino, SIGNAL(OffEvent()), &frameGrabber, SLOT(setGrabbingOff()), Qt::QueuedConnection);
    QObject::connect(&arduino, SIGNAL(IrEvent()), &frameGrabber, SLOT(grabFramesRemote()), Qt::QueuedConnection);

    QObject::connect(&frameGrabber, SIGNAL(finished()), &grabberThread, SLOT(quit()));
    QObject::connect(&frameProcessor, SIGNAL(finished()), &processorThread, SLOT(quit()));
    QObject::connect(&frameSender, SIGNAL(finished()), &senderThread, SLOT(quit()));
    QObject::connect(&controller, SIGNAL(finished()), &controlThread, SLOT(quit()));

    QObject::connect(&grabberThread, SIGNAL(finished()), this, SLOT(threadFinished()));
    QObject::connect(&processorThread, SIGNAL(finished()), this, SLOT(threadFinished()));
    QObject::connect(&senderThread, SIGNAL(finished()), this, SLOT(threadFinished()));
    QObject::connect(&controlThread, SIGNAL(finished()), this, SLOT(threadFinished()));

    QObject::connect(&frameGrabber,  SIGNAL(hadEvent(StatEvent)), &controller, SLOT(addEvent(StatEvent)));
    QObject::connect(&frameProcessor, SIGNAL(hadEvent(StatEvent)), &controller, SLOT(addEvent(StatEvent)));
    QObject::connect(&frameSender, SIGNAL(hadEvent(StatEvent)), &controller, SLOT(addEvent(StatEvent)));
    QObject::connect(&controller, SIGNAL(hadEvent(StatEvent)), &controller, SLOT(addEvent(StatEvent)));

    frameGrabber.moveToThread(&grabberThread);
    frameProcessor.moveToThread(&processorThread);
    frameSender.moveToThread(&senderThread);
    controller.moveToThread(&controlThread);

    grabberThread.start();
    processorThread.start();
    senderThread.start();
    controlThread.start();
}

void Dispatcher::startCapturing(){
    frameGrabber.startTimerLoop();
}

void Dispatcher::intSignalHandler(int)
{
    char a = 1;
    if(-1 == ::write(sigintpFd[0], &a, sizeof(a))) qDebug() << "unexpected Socket ERR";
    qDebug() << "Caught signal intSignalHandler";
}

void Dispatcher::termSignalHandler(int)
{
    char a = 1;
    if(-1 ==  ::write(sigtermFd[0], &a, sizeof(a))) qDebug() << "unexpected Socket ERR";
    qDebug() << "Caught signal termSignalHandler";

}

void Dispatcher::handleSigTerm()
{
    snTerm->setEnabled(false);
    char tmp;
    if(-1 == ::read(sigtermFd[1], &tmp, sizeof(tmp))) qDebug() << "unexpected Socket ERR";

    // do Qt stuff
    qDebug() << "Caught signal handleSigTerm";
    forceClose = true;
    close();
    snTerm->setEnabled(true);
}

void Dispatcher::handleSigInt()
{
    snInt->setEnabled(false);
    char tmp;
    if(-1 == ::read(sigintpFd[1], &tmp, sizeof(tmp))) qDebug() << "unexpected Socket ERR";
    // do Qt stuff
    qDebug() << "Caught signal handleSigInt";
    close();
    snInt->setEnabled(true);
}

void Dispatcher::threadFinished()
{
    if(!doClose) return;
    qDebug() << "Caught signal threadFinished";
    if(controlThread.isFinished() && !grabberThread.isFinished()){
        qDebug() << "send termiate to grabberThread";
        QMetaObject::invokeMethod(&frameGrabber, "doTerminate", Qt::QueuedConnection, Q_ARG( bool, forceClose));
    }else if(grabberThread.isFinished() && !processorThread.isFinished()){
        qDebug() << "send termiate to processorThread";
        QMetaObject::invokeMethod(&frameProcessor, "doTerminate", Qt::QueuedConnection, Q_ARG( bool, forceClose));
    }else if(processorThread.isFinished() && !senderThread.isFinished()){
        qDebug() << "send termiate to senderThread";
        QMetaObject::invokeMethod(&frameSender, "doTerminate", Qt::QueuedConnection, Q_ARG( bool, forceClose));
    }else if(senderThread.isFinished()&& !controlThread.isFinished()){
        qDebug() << "send termiate to controlThread";
        QMetaObject::invokeMethod(&controller, "doTerminate", Qt::QueuedConnection, Q_ARG( bool, forceClose));
    }else{
        quit();
    }
}

void Dispatcher::close()
{
    doClose = true;
    if(closeAttempts > 0) forceClose = true;
    qDebug() << "================================ CLOSE force = " << forceClose;

    if(controlThread.isRunning()){
        qDebug() << "================================ CLOSE controlThread running -> send doTerminate";
        QMetaObject::invokeMethod(&controller, "doTerminate", Qt::QueuedConnection, Q_ARG( bool, forceClose));
    }

    if(forceClose){
        if(controlThread.isRunning()) QMetaObject::invokeMethod(&controller, "doTerminate", Qt::QueuedConnection, Q_ARG( bool, forceClose));
        if(grabberThread.isRunning()) QMetaObject::invokeMethod(&frameGrabber, "doTerminate", Qt::QueuedConnection, Q_ARG( bool, forceClose));
        if(processorThread.isRunning()) QMetaObject::invokeMethod(&frameProcessor, "doTerminate", Qt::QueuedConnection, Q_ARG( bool, forceClose));
        if(senderThread.isRunning()) QMetaObject::invokeMethod(&frameSender, "doTerminate", Qt::QueuedConnection, Q_ARG( bool, forceClose));
    }
    closeAttempts++;
}

void Dispatcher::reload(SmartSettings settings)
{
    qDebug() << "Commander reload";
    this->settings = settings;
    QMetaObject::invokeMethod(&frameGrabber, "load", Qt::QueuedConnection, Q_ARG( SmartSettings, settings));
    qDebug() << "Commander reload frameGrabber";
    QMetaObject::invokeMethod(&frameProcessor, "load", Qt::QueuedConnection, Q_ARG( SmartSettings, settings));
    qDebug() << "Commander reload frameProcessor";
    QMetaObject::invokeMethod(&frameSender, "load", Qt::QueuedConnection, Q_ARG( SmartSettings, settings));
    qDebug() << "Commander reload frameSender";
    QMetaObject::invokeMethod(&controller, "load", Qt::QueuedConnection, Q_ARG( SmartSettings, settings));
    qDebug() << "Commander reload controller";
    QMetaObject::invokeMethod(&frameGrabber, "startTimerLoop", Qt::QueuedConnection);
    this->settings.dumpToFile("settings.json");
}

SmartSettings Dispatcher::getSettings()
{
    return settings;
}

int Dispatcher::setup_unix_signal_handlers()
{
    struct sigaction sint, term;
    qDebug() << "setup_unix_signal_handlers";
    sint.sa_handler = Dispatcher::intSignalHandler;
    sigemptyset(&sint.sa_mask);
    sint.sa_flags = 0;
    sint.sa_flags |= SA_RESTART;

    if (sigaction(SIGINT, &sint, 0) > 0) return 1;

    term.sa_handler = Dispatcher::termSignalHandler;
    sigemptyset(&term.sa_mask);
    term.sa_flags |= SA_RESTART;

    if (sigaction(SIGTERM, &term, 0) > 0) return 2;

    return 0;
}
