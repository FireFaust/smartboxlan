#include "commandandcontrol.h"

CommandAndControl::CommandAndControl(QObject *parent) :
    HttpRequestHandler(parent)
{
    listener = 0;
    udpSocket = 0;
    frameGrabber = 0;
    frameProcessor = 0;
    frameSender = 0;
    dispacher = 0;
    regionOptDownloader = 0;
    port = 9898;
    udpRebindTimer = new QTimer(this);
    regionOptTimer = new QTimer(this);

    webSettings.setValue("port",port);
    webSettings.setValue("minThreads",1);
    webSettings.setValue("maxThreads",1000);
    webSettings.setValue("cleanupInterval",1000);
    webSettings.setValue("readTimeout",60000);
    webSettings.setValue("maxRequestSize",16000);
    webSettings.setValue("maxMultiPartSize",1000000);

    udpGroupAddress = QHostAddress("239.255.39.23");
    connect(udpRebindTimer,SIGNAL(timeout()),this,SLOT(bindUdp()));
    connect(regionOptTimer,SIGNAL(timeout()),this,SLOT(requestRegionOpt()));

    qDebug() << " globSettings =  " << globSettings.value("SMB/id","unkn").toString();
    reload();
}

CommandAndControl::~CommandAndControl()
{
    if(listener != 0)
    {
        delete listener;
    }
}

void CommandAndControl::load(SmartSettings settings)
{
    //qDebug() << "CommandAndControl::load --------------------";
    this->settings = settings;
    reload();
}

void CommandAndControl::reload()
{
    //qDebug() << "CommandAndControl::reload";
    udpRebindTimer->stop();
    if(listener != 0) delete listener;
    listener = new HttpListener(&webSettings,this,this);
    udpRebindTimer->start(5000);
    regionOptTimer->start(settings.rectUpdateIntervalS*1000);
}

void CommandAndControl::doTerminate(bool force)
{
    qDebug() << "CommandAndControl::doTerminate";
    if(listener != 0)
    {
        delete listener;
        listener = 0;
    }
    emit finished();
}


void CommandAndControl::requestRegionOpt()
{
    if(regionOptDownloader != 0) return;
    QUrl url(settings.serverUrl);
    QString rectPath = settings.sender.rectGetUrl.arg(globSettings.value("SMB/id","125").toString());
    url.setPath(rectPath);
    //qDebug() << "Request RGION RECT url: " << url;
    regionOptDownloader = new SmartFileDownloader(url.toString(),this);

    connect(regionOptDownloader,SIGNAL(finished(SmartFileDownloader*)),this,SLOT(requestRegionFinished(SmartFileDownloader*)));
    connect(regionOptDownloader,SIGNAL(failed(SmartFileDownloader*)),this,SLOT(requestRegionFailed(SmartFileDownloader*)));
    regionOptDownloader->send();
}

void CommandAndControl::requestRegionFinished(SmartFileDownloader * dl)
{
    //qDebug() << "CommandAndControl::requestRegionFinished---------------";
    QByteArray result = dl->getResult();
    regionOptDownloader=0;
    dl->deleteLater();

    RegionSettings regionSettings;
    if(!regionSettings.fromByteArray(result)) return;

    SmartSettings tSettings = settings;
    if(tSettings.processor.regionSettings == regionSettings) return;
    tSettings.processor.regionSettings = regionSettings;

    QMetaObject::invokeMethod(dispacher, "reload", Qt::QueuedConnection, Q_ARG(SmartSettings, tSettings));
}

void CommandAndControl::requestRegionFailed(SmartFileDownloader * dl)
{
    qDebug() << "!! CommandAndControl::requestRegionFailed  Network errror";
    regionOptDownloader = 0;
    dl->deleteLater();
}


void CommandAndControl::service(HttpRequest& request, HttpResponse& response) {
    QByteArray path=request.getPath();
    //qDebug("HTTP: path=%s",path.data());
    response.setHeader("Access-Control-Allow-Origin", "*");

    if (path.startsWith("/stat")) {
        httpStat(request,response);
    }

    else if (path.startsWith("/grab")) {
        httpGetFrame(request, response);
    }

    else if (path.startsWith("/settings")) {
        httpSettings(request, response);
    }

    else if (path.startsWith("/face")) {
        httpGetFace(request, response);
    }
    else if (path.startsWith("/scanid")) {
        httpScanid(request, response);
    }
    else {
        httpDefault(request,response);
    }
}
void CommandAndControl::httpScanid(HttpRequest& request, HttpResponse& response){
    QList<QHostAddress> list = QNetworkInterface::allAddresses();
    QByteArray resp="SmBox";
    for(int nIter=0; nIter<4; nIter++)
    {
        if(list[nIter].isLoopback()) continue;
        if(list[nIter].protocol() == QAbstractSocket::IPv4Protocol){
            resp.append(list[nIter].toString());
        }
    }

    response.write(resp,true);
}

void CommandAndControl::httpDefault(HttpRequest& request, HttpResponse& response) {
    QByteArray path=request.getPath();
    if(path == "/" || path.length() == 0) path = "index.html";
    QFile file(QString(":/www/res/www/").append(path));
    if(!file.exists()) return;
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;
    QByteArray html;
    html = file.readAll();
    if(path.endsWith(".jpg")) response.setHeader("Content-Type", "image/jpeg");
    if(path.endsWith(".ico")) response.setHeader("Content-Type", "image/x-icon");
    response.write(html,true);
}

void CommandAndControl::httpStat(HttpRequest& request __attribute__ ((unused)), HttpResponse& response) {
    /*float fpm;
    QMetaObject::invokeMethod(frameProcessor, "fpm", Qt::BlockingQueuedConnection, Q_RETURN_ARG(float, fpm));
    response.write("<html><body>");
    response.write(QString("FPM: %1").arg( QString::number(fpm, 'f', 2)).toStdString().c_str());
    response.write("</body></html>",true);*/

    QJsonObject stats;
    stats.insert("started",(double)SmartLog::instance()->getStarted());
    stats.insert("logs",SmartLog::instance()->getLastLogs().toArray());
    stats.insert("faces",SmartHistory::instance()->getLastFaces().toArray());
    response.write(QJsonDocument(stats).toJson(),true);
}

void CommandAndControl::httpGetFrame(HttpRequest& request, HttpResponse& response) {
    QMutexLocker locker(&mutex);

    struct timespec requestStart, requestEnd;
    clock_gettime(CLOCK_REALTIME, &requestStart);


    if(frameGrabber == 0){
        response.setStatus(404,"Not Found");
        response.write("",true);
        return;
    }

    int source = 0;
    QByteArray path=request.getPath();
    if(path.contains("tv")) source = 1;

    QByteArray jpeg;
    QMetaObject::invokeMethod(frameGrabber, "getFrame", Qt::BlockingQueuedConnection, Q_RETURN_ARG(QByteArray, jpeg),Q_ARG(int,source));
    if(jpeg.size() == 0){
        response.setStatus(404,"Not Found");
        response.write("",true);
        return;
    }
    response.setHeader("Content-Type", "image/jpeg");
    response.write(jpeg,true);

    clock_gettime(CLOCK_REALTIME, &requestEnd);
    double accum = ( requestEnd.tv_sec - requestStart.tv_sec ) + ( requestEnd.tv_nsec - requestStart.tv_nsec ) / BILLION;
    if(accum < 0) accum = (requestEnd.tv_sec - requestStart.tv_sec) / BILLION;
    qDebug() << "Remote frame grabbed in                                     " << QString::number(accum,'f',2).toStdString().c_str() << " ms";
}

void CommandAndControl::httpGetFace(HttpRequest& request, HttpResponse& response) {
    qint64 id = -1;
    QString path = request.getPath();
    if(path.length() > 6){
        path = path.right(path.length()-6);
        bool ok;
        id = path.toLong(&ok, 10);
        if(!ok) id = -1;
    }

    SmartHistory * hist = SmartHistory::instance();
    QByteArray jpeg = hist->getFace(id);
    response.setHeader("Content-Type", "image/jpeg");
    response.write(jpeg,true);
}

void CommandAndControl::httpSettings(HttpRequest& request, HttpResponse& response) {
    QMutexLocker locker(&mutex);
    SmartSettings settings;

    QByteArray tmp;
    if (request.getParameter("action")=="update_settings") {
        settings.fromByteArray(request.getParameter("settings"));
        QMetaObject::invokeMethod(dispacher, "reload", Qt::QueuedConnection, Q_ARG(SmartSettings, settings));
        response.write("<html><head><meta http-equiv=\"refresh\" content=\"5\" ></head><body>Reloading... in <span id=\"time\">5</span> sec<script>var t=5;setInterval(function tick() {t--;document.getElementById(\"time\").innerHTML = t;},1000);</script></body></html>",true);
        return;
    }

    QMetaObject::invokeMethod(dispacher, "getSettings", Qt::BlockingQueuedConnection, Q_RETURN_ARG(SmartSettings, settings));

    response.write("<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"/style.css\" /></head><body>");
    response.write("<form method=\"post\" class=\"settings\">");
    response.write("  <input type=\"hidden\" name=\"action\" value=\"update_settings\">");
    response.write("  <textarea name=\"settings\">");
    response.write(settings.toByteArray());
    response.write("  </textarea>");
    response.write("  <input type=\"submit\">");
    response.write("</form>");
    response.write("</body></html>",true);
}

void CommandAndControl::addEvent(StatEvent event)
{
    events.addEvent(event);
}

void CommandAndControl::processUdpDatagrams()
{
    while (udpSocket->hasPendingDatagrams()) {
        QByteArray request;
        request.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(request.data(), request.size());

        if(request.startsWith("SMBWIZ")){
            QList<QHostAddress> list = QNetworkInterface::allAddresses();
            for(int nIter=0; nIter<list.count(); nIter++)
            {
                if(list[nIter].isLoopback()) continue;
                QByteArray response = "SMBSERV";
                if(list[nIter].protocol() == QAbstractSocket::IPv4Protocol){
                    response.append(list[nIter].toString());
                }else{
                    continue;
                    Q_IPV6ADDR ipv6 = list[nIter].toIPv6Address();
                    quint16 ugle[8];
                    for (int i = 0; i < 8; i++) {
                        ugle[i] = (quint16(ipv6[2*i]) << 8) | quint16(ipv6[2*i+1]);
                    }
                    QString s;
                    s.sprintf("%X:%X:%X:%X:%X:%X:%X:%X", ugle[0], ugle[1], ugle[2], ugle[3], ugle[4], ugle[5], ugle[6], ugle[7]);
                    response.append(s);
                }

                udpSocket->writeDatagram(response.data(), response.size(),udpGroupAddress, port);
            }
        }

        qDebug() << tr("Received datagram: \"%1\" ").arg(request.data());
    }
}

void CommandAndControl::bindUdp()
{
    if(udpSocket != 0) delete udpSocket;
    udpSocket = new QUdpSocket(this);
    udpSocket->bind(QHostAddress::AnyIPv4, port, QUdpSocket::ShareAddress);
    udpSocket->joinMulticastGroup(udpGroupAddress);
    connect(udpSocket, SIGNAL(readyRead()),this, SLOT(processUdpDatagrams()));
}
void CommandAndControl::cUrl()
{
    QUrl url = QUrl("localhost/scanid");
    //url.setFragment("Hello",QUrl::StrictMode);
}
