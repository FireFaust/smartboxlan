TEMPLATE = subdirs

SUBDIRS += Capture
SUBDIRS += TestParse
SUBDIRS += Wizzard
SUBDIRS += PicParse

Capture.depends = PicParse
TestParse.depends = PicParse
