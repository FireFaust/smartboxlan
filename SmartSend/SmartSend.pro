#-------------------------------------------------
#
# Project created by QtCreator 2013-11-11T09:46:12
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = SmartSend
TEMPLATE = lib
CONFIG += staticlib

SOURCES += smartsend.cpp

HEADERS += smartsend.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
