#ifndef SMARTSEND_H
#define SMARTSEND_H

#include <QThread>
#include <QTimer>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QUrl>
#include <QHttpMultiPart>
#include <QFile>

#include <iostream>
#include <stdio.h>


class SmartSend: public QThread
{
    Q_OBJECT
public:
    explicit SmartSend(QObject *parent = 0);
    void run();

signals:

public slots:
private slots:
    void trySend();
    void imagesSent(QNetworkReply *);
private:
    QTimer *timer;
    int whitch;
};

#endif // SMARTSEND_H
