#include "smartsend.h"

SmartSend::SmartSend(QObject *parent) :
    QThread(parent)
{
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(trySend()));
    whitch = 0;
}

void SmartSend::run()
{
    // Start event loop
    timer->start(1000);
    fprintf(stderr, "Timer start");
    exec();
}

void SmartSend::trySend(){
    fprintf(stderr, "Try send");
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url("http://localhost/tupload.php");
    QNetworkRequest request(url);

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart textPart;
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"text\""));
    textPart.setBody("my text");

    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"image\"; filename=\"image.jpg\"")); // <<< filename triggers separate handling
    QFile *file = new QFile("image.jpg");
    file->open(QIODevice::ReadOnly);
    imagePart.setBodyDevice(file);
    file->setParent(multiPart);

    QHttpPart imagePart2;
    imagePart2.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
    imagePart2.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"image2\"; filename=\"image2.jpg\"")); // <<< filename triggers separate handling
    QFile *file2 = new QFile("image.jpg");
    file2->open(QIODevice::ReadOnly);
    imagePart2.setBodyDevice(file2);
    file2->setParent(multiPart);

    multiPart->append(textPart);
    multiPart->append(imagePart);
    multiPart->append(imagePart2);

    QObject::connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(imagesSent(QNetworkReply *)));
    manager->post(request, multiPart);
}

void SmartSend::imagesSent(QNetworkReply *reply){
    fprintf(stderr, "imagesSent");
}
