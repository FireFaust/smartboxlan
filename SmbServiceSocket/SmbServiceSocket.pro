#-------------------------------------------------
#
# Project created by QtCreator 2014-01-04T12:49:31
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = SmbServiceSocket
TEMPLATE = lib
CONFIG += staticlib

SOURCES += smbservicesocket.cpp

HEADERS += smbservicesocket.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
