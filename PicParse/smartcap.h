#ifndef SMARTCAP_H
#define SMARTCAP_H

#include <QMutex>
#include <QMutexLocker>

#include "flycap.h"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "v4l2cap.h"

enum SMART_CAMERA_SRC{
    OPENCV_CAP = 0,
    FLYCAP_CAP = 1,
    VIDEO_CAP = 2,
    EASY_CAP = 3
};

class SmartCap
{
public:
    SmartCap(SMART_CAMERA_SRC camera_src, int camera_number = 0);
    SmartCap(const char * filename);
    ~SmartCap();

    bool ok();
    cv::Mat * getFrame();
    bool prepareFrame();
    void init();
    void setCamera();
    const char * error;

private:
    SMART_CAMERA_SRC camera_src;
    FlyCap * flyCap;
    cv::VideoCapture * cvCap;
    V4L2Cap * v4lCap;
    void dispose();
    int camera_number;
    const char * filename;

    mutable QMutex mutex;
};

#endif // SMARTCAP_H
