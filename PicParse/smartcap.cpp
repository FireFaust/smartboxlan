#include "smartcap.h"

SmartCap::SmartCap(SMART_CAMERA_SRC camera_src, int camera_number)
{
    if(camera_src == VIDEO_CAP){
        qCritical("Wrong camera source VIDEO_CAP");
        camera_src = OPENCV_CAP;
    }
    this->camera_src = camera_src;
    this->camera_number = camera_number;

    flyCap = 0;
    cvCap = 0;
    v4lCap = 0;
    init();
}

SmartCap::SmartCap(const char * filename){
    this->camera_src = VIDEO_CAP;
    this->filename = filename;
    flyCap = 0;

    cvCap = 0;
    v4lCap = 0;
    init();
}

SmartCap::~SmartCap()
{
    QMutexLocker locker(&mutex);
    dispose();
}

void SmartCap::init(){
    QMutexLocker locker(&mutex);
    switch(camera_src){
    case FLYCAP_CAP:
        if(flyCap != 0) delete flyCap;
        flyCap = new FlyCap();
        break;
    case OPENCV_CAP:
        if(cvCap != 0) delete cvCap;
        cvCap = new cv::VideoCapture(this->camera_number);
        if(!cvCap || !cvCap->isOpened()){
            qCritical("Failed to open/init video stream!");
        }
        break;
    case VIDEO_CAP:
        if(cvCap != 0) delete cvCap;
        cvCap = new cv::VideoCapture(filename);
        if(!cvCap){
            qCritical("Failed to open/init video file %s", filename);
        }
        break;
    case EASY_CAP:
        if(v4lCap == 0){
            qDebug() << " CAMERA NUMBER = " << this->camera_number;
            v4lCap = new V4L2Cap(this->camera_number);
            if(!v4lCap || !v4lCap->isOk()){
                qWarning() << "Failed to init v4lCap capture! Error: " << v4lCap->error;
            }
        }else{
            v4lCap->reload();
        }
        break;
    }
}

void SmartCap::dispose()
{
    switch(camera_src){
    case FLYCAP_CAP:
        if(flyCap != 0) delete flyCap;
        flyCap = 0;
        break;
    case VIDEO_CAP:
    case OPENCV_CAP:
        if(cvCap != 0) delete cvCap;
        cvCap = 0;
        break;
    case EASY_CAP:
        if(v4lCap != 0) delete v4lCap;
        v4lCap = 0;
        break;
    }
}

bool SmartCap::ok(){
    QMutexLocker locker(&mutex);
    switch(camera_src){
    case FLYCAP_CAP:
        if(flyCap == 0) return false;
        return flyCap->ok();
        break;
    case VIDEO_CAP:
        if(cvCap == 0) return false;
        return cvCap->isOpened();
        break;
    case OPENCV_CAP:
        if(cvCap == 0) return false;
        return cvCap->isOpened();
        break;
    case EASY_CAP:
        if(v4lCap == 0) return false;
        return v4lCap->isOk();
        break;
    }
    return false;
}

cv::Mat * SmartCap::getFrame(){
    QMutexLocker locker(&mutex);
    cv::Mat * clone;
    cv::Mat result;
    switch(camera_src){
    case FLYCAP_CAP:
        if(flyCap == 0) return new cv::Mat();
        return flyCap->getFrame();
        break;
    case VIDEO_CAP:
        if(cvCap == 0) return new cv::Mat();
        try
    {
        (*cvCap) >> result;
    }
        catch(cv::Exception ex)\
        {
            qDebug() << "cv::Exception " << ex.what();
        }
        catch(...)
    {
        qDebug() << "Unknown exception in SmartCap";
    }

        clone = new cv::Mat();
        if(result.type() != CV_8UC1){
            cv::cvtColor( result, *clone, cv::COLOR_BGR2GRAY );
        }else{
            result.copyTo(*clone);
        }
        return clone;
        break;
    case OPENCV_CAP:
        if(cvCap == 0) return new cv::Mat();
        try
    {
        cvCap->read(result);
    }
        catch(cv::Exception ex)\
        {
            qDebug() << "cv::Exception " << ex.what();
        }
        catch(...)
    {
        qDebug() << "Unknown exception in smartCap";
    }

        clone = new cv::Mat();
        if(result.type() != CV_8UC1){
            cv::cvtColor( result, *clone, cv::COLOR_BGR2GRAY );
        }else{
            result.copyTo(*clone);
        }
        return clone;
        break;
    case EASY_CAP:
        if(v4lCap == 0){
            qDebug() << "v4lCap == 0";
            return new cv::Mat();
        }
        if(!v4lCap->isOk()){
            qDebug() << "!v4lCap->isOk";
            return new cv::Mat();
        }
        char * d = v4lCap->grab();
        if(d == 0){
            qDebug() << "Failed to grab";
            return new cv::Mat();
        }
        cv::Mat * result = new cv::Mat(v4lCap->getHeight(),v4lCap->getWidth(),CV_8UC1,d);
        return result;
        break;
    }
    return 0;
}

bool SmartCap::prepareFrame(){
    QMutexLocker locker(&mutex);
    switch(this->camera_src){
    case FLYCAP_CAP:
        if(flyCap == 0) return false;
        return true;
//        return flyCap->prepareFrame();
        break;
    case VIDEO_CAP:
        if(cvCap == 0) return false;
        return true;
        break;
    case OPENCV_CAP:
        if(cvCap == 0) return false;
        try
    {
        cvCap->grab();
    }
        catch(cv::Exception ex)
        {
            qDebug()<< " cv::Exception "<< ex.what();
            return false;
        }
        catch(...)
    {
        qDebug(" Unknown exception " );
        return false;
    }

        return true;
        break;

    case EASY_CAP:
        return v4lCap->isOk();
        break;
    }
    return false;
}




