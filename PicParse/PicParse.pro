QT       -= gui
QT       += network

TARGET = PicParse
TEMPLATE = lib
#CONFIG += staticlib

DEFINES += PICPARSE_LIBRARY

INCLUDEPATH += /usr/include
LIBS += -L/usr/lib

#DEFINES += HAS_FLYCAP
SOURCES += flycap.cpp \
    settings/regionsettings.cpp
HEADERS += flycap.h \
    settings/regionsettings.h

LIBS += -lflycapture
LIBS += -ljpeg

SOURCES += picparse.cpp \
    parsedfaces.cpp \
    smartcap.cpp \
    settings/smartsettings.cpp \
    settings/optcapsource.cpp \
    settings/opt.cpp \
    settings/optprocessor.cpp \
    settings/optcascade.cpp \
    smartconv.cpp \
    settings/optsender.cpp \
    v4l2cap.cpp \
    settings/optrect.cpp \
    settings/optsize.cpp \
    facelearn.cpp


HEADERS += picparse.h\
        picparse_global.h \
    parsedfaces.h \
    smartcap.h \
    settings/smartsettings.h \
    settings/optcapsource.h \
    settings/opt.h \
    settings/optprocessor.h \
    settings/optcascade.h \
    smartconv.h \
    settings/optsender.h \
    v4l2cap.h \
    settings/optrect.h \
    settings/optsize.h \
    facelearn.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core

RESOURCES += \
    Xmls.qrc

