#include "facelearn.h"

FaceLearn::FaceLearn()
{
    init();
}

FaceLearn::~FaceLearn()
{
    clear();
}

void FaceLearn::init()
{
    model = cv::createLBPHFaceRecognizer(1,8,6,6,30);
    trainedFaces = 0;

    stdDevHistoryMax = 10;
    //stdDevMax = 4;
    avgStdDevMax = 1;
    minStdPercent = 1;
    stdPercentMul = 30;
}


void FaceLearn::clear()
{

    for(std::vector<cv::Mat *>::iterator it = originalFaces.begin(); it != originalFaces.end(); ++it)
    {
        delete *it;
    }

    for(std::vector<cv::Mat *>::iterator it = scaledFaces.begin(); it != scaledFaces.end(); ++it)
    {
        delete *it;
    }

    scaledFaces.clear();
    originalFaces.clear();
    trainedFaces = 0;
    stdDevArr.clear();
    stdDevAvgArr.clear();
}


void FaceLearn::saveToFile(QString filename)
{
    if(trainedFaces == 0) return;
    model->save(filename.toStdString());
}

double FaceLearn::addFace(QByteArray img)
{
    cv::Mat * mat = SmartConv::JpgToMatP(&img);
    if(mat->empty()) {
        delete mat;
        return 0;
    }
    return addFace(mat);
}

double FaceLearn::addFace(cv::Mat * img)
{
    cv::Mat * face = new cv::Mat();
    cv::resize(*img,*face,cv::Size(100,100));
    double confidence = 0;
    double percent = minStdPercent;
    int label;
    if(trainedFaces > 2){
        model->predict(*face,label,confidence);
        //qDebug() << "P " << label << " " << confidence << " " << trainedFaces;
        double tStdDev = stdDev(confidence);
        double tAvgStdDev = avgStdDev(tStdDev);
        addAvgDev(tStdDev);
        addDev(confidence);

        if(label != -1 && tAvgStdDev < avgStdDevMax){

            qDebug() << 100 << "     " <<confidence << "             " <<tStdDev<< "             " << tAvgStdDev;
           // qDebug() << "============== OK ===============";
            delete face;
            delete img;
            return 100;
        }

        if(label != -1){
            percent = std::max(0.,std::min(avgStdDevMax*stdPercentMul, avgStdDevMax*(stdPercentMul+1)-tAvgStdDev))/(avgStdDevMax*stdPercentMul);
            percent = percent * (100-minStdPercent)+minStdPercent;
            qDebug() << percent << "     " << confidence << "             " <<tStdDev<< "             " << tAvgStdDev;
        }
    }
    originalFaces.push_back(img);
    scaledFaces.push_back(face);
    train();
    return percent;
}


void FaceLearn::train()
{
    if(scaledFaces.empty()) return;
    std::vector<cv::Mat> tFaces;
    std::vector<int> tLabels;

    if(scaledFaces.size() < 3){
        for(std::vector<cv::Mat *>::iterator it = scaledFaces.begin(); it != scaledFaces.end(); ++it)
        {
            tFaces.push_back(*(*it));
            tLabels.push_back(0);
        }
        model->train(tFaces, tLabels);
        trainedFaces = scaledFaces.size();
    }else if(scaledFaces.size() > trainedFaces){
        for(std::vector<cv::Mat *>::iterator it = scaledFaces.begin()+scaledFaces.size()-trainedFaces; it != scaledFaces.end(); ++it)
        {
            tFaces.push_back(*(*it));
            tLabels.push_back(0);
        }
        model->update(tFaces, tLabels);
        trainedFaces = scaledFaces.size();
    }
}

double FaceLearn::avgStdDev()
{
    if(stdDevArr.empty()) return 0;
    double total = 0;
    for(std::deque<double>::iterator it = stdDevArr.begin(); it != stdDevArr.end(); ++it)
    {
        total += *it;
    }
    return total/stdDevArr.size();
}

double FaceLearn::avgAvgStdDev()
{
    if(stdDevAvgArr.empty()) return 0;
    double total = 0;
    for(std::deque<double>::iterator it = stdDevAvgArr.begin(); it != stdDevAvgArr.end(); ++it)
    {
        total += *it;
    }
    return total/stdDevAvgArr.size();
}

std::vector<QByteArray *> FaceLearn::getFaces()
{
    std::vector<QByteArray *> res;
    QByteArray * tFace;
    for(std::vector<cv::Mat *>::iterator it = originalFaces.begin(); it != originalFaces.end(); ++it)
    {
        tFace = SmartConv::MatToJpgP(*(*it));
        res.push_back(tFace);
    }
    return res;
}
