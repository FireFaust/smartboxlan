#include "flycap.h"

FlyCap::FlyCap()
{
    cameraID = 0;
    tryConnect();
}

FlyCap::~FlyCap()
{
    disconnect();
}

bool FlyCap::tryConnect()
{
    FlyCapture2::Error error;

    FlyCapture2::BusManager busMgr;
    unsigned int numCameras;
    error = busMgr.GetNumOfCameras(&numCameras);
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << error.GetDescription();
        return false;
    }
    qDebug() << "Number of cameras detected: " << numCameras;
    if(numCameras < cameraID+1) return false;

    FlyCapture2::PGRGuid guid;
    error = busMgr.GetCameraFromIndex(cameraID, &guid);
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << error.GetDescription();
        return false;
    }

    // Connect the camera
    error = camera.Connect( &guid );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        qWarning() << "Failed to connect to FlyCap camera";
        return false;
    }

    // Get the camera information
    FlyCapture2::CameraInfo camInfo;
    error = camera.GetCameraInfo(&camInfo);
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << error.GetDescription();
        return false;
    }

    camera.SetVideoModeAndFrameRate(FlyCapture2::VIDEOMODE_1280x960Y8, FlyCapture2::FRAMERATE_15);

    // Get current trigger settings
    FlyCapture2::TriggerMode triggerMode;
    error = camera.GetTriggerMode( &triggerMode );
    if (error != FlyCapture2::PGRERROR_OK) {
        qCritical() << error.GetDescription();
        return false;
    }
    // Set camera to trigger mode 0
    triggerMode.onOff = false;
    triggerMode.mode = 0;
    triggerMode.parameter = 0;
    // A source of 7 means software trigger
    triggerMode.source = 0;
    error = camera.SetTriggerMode( &triggerMode );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        qWarning("Failed to set trigger mode for FlyCap camera");
        return false;
    }


    // Get the camera configuration
    FlyCapture2::FC2Config config;
    error = camera.GetConfiguration(&config);
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << error.GetDescription() ;
        return false;
    }

    config.grabTimeout = FlyCapture2::TIMEOUT_INFINITE;

    // Set the camera configuration
    error = camera.SetConfiguration(&config);
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << error.GetDescription();
        return false;
    }

    /*qDebug << camInfo.vendorName << " "
           << camInfo.modelName << " "
           << camInfo.serialNumber << std::endl;*/
/*
    if (!CheckSoftwareTriggerPresence()) {
        printf( "SOFT_ASYNC_TRIGGER not implemented on this camera!  Stopping application\n");
        return false;
    }


    if(!CheckTriggerReady())
    {
        qCritical("Failed to check trigger for FlyCap camera");
        return false;
    }
*/
    error = camera.StartCapture();


    if ( error == FlyCapture2::PGRERROR_ISOCH_BANDWIDTH_EXCEEDED )
    {
        qWarning("Bandwidth exceeded from FlyCap camera");
        return false;
    }
    else if ( error != FlyCapture2::PGRERROR_OK )
    {
        qWarning("Failed to start image capture from FlyCap camera");
        return false;
    }

    return true;
}

bool FlyCap::CheckSoftwareTriggerPresence() {
    using FlyCapture2::PGRERROR_OK;
    const unsigned int k_triggerInq = 0x530;

    FlyCapture2::Error error;
    unsigned int regVal = 0;

    error = camera.ReadRegister(k_triggerInq, &regVal);

    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning() << error.GetDescription();
        return false;
    }

    if(( regVal & 0x10000 ) != 0x10000) {
        return false;
    }
    return true;
}

bool FlyCap::prepareFrame()
{
   // CheckTriggerReady();
   /* const unsigned int k_softwareTrigger = 0x62C;
    const unsigned int k_fireVal = 0x80000000;
    FlyCapture2::Error error;

    error = camera.WriteRegister(k_softwareTrigger, k_fireVal);
    if (error != FlyCapture2::PGRERROR_OK) {
        qWarning("Failed to set trigger for FlyCap camera");
        return false;
    }
    */
    return true;
}

cv::Mat * FlyCap::getFrame(){
    FlyCapture2::Image rawImage;
    FlyCapture2::Error error = camera.RetrieveBuffer( &rawImage );
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        qWarning("Capture failed from FlyCap camera");
        cv::Mat * result = new cv::Mat();
        return result;
    }
    // convert to gray
    // convert to OpenCV Mat
    //unsigned int rowBytes = (double)rawImage.GetReceivedDataSize()/(double)rawImage.GetRows();
    //unsigned char * copyData = (unsigned char*)malloc(rawImage.GetDataSize());
    cv::Mat * result = new cv::Mat( rawImage.GetRows(), rawImage.GetCols(), CV_8UC1 );
    memcpy( result->data, rawImage.GetData(), rawImage.GetDataSize() );
    rawImage.ReleaseBuffer();
    return result;
}

bool FlyCap::CheckTriggerReady() {
    /*using FlyCapture2::PGRERROR_OK;
    const unsigned int k_softwareTrigger = 0x62C;
    FlyCapture2::Error error;
    unsigned int regVal = 0;
    const int max_fails = 50;
    int fails = 0;
    do {
        error = camera.ReadRegister(k_softwareTrigger, &regVal);
        if (error != PGRERROR_OK) {
           qWarning() << error.GetDescription();
            return false;
        }
        fails ++;
    } while ((regVal >> 31) != 0 && fails < max_fails);
*/
    return true;
}

void FlyCap::disconnect(){
    if(!camera.IsConnected()) return;
    FlyCapture2::Error error = camera.StopCapture();
    if ( error != FlyCapture2::PGRERROR_OK )
    {
        qWarning() << "FlyCap camera prematurely disconnected";
    }
    camera.Disconnect();
}

bool FlyCap::ok()
{
    return camera.IsConnected();
}
