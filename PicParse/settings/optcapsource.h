#ifndef OPTCAPSOURCE_H
#define OPTCAPSOURCE_H

#include "smartcap.h"
#include "opt.h"
#include <QFile>

class OptCapSource: Opt
{   
public:
    OptCapSource();
    //SmartCap * createCapSource();

    bool setJSON(QJsonValue value);
    QJsonValue getJSON();
    void setDefaults();

    int sourceType;
    int sourceNum;
    QString filename;

    bool operator==(OptCapSource &other);
    inline bool operator!=(OptCapSource &other){return !(*this == other);}
};

#endif // OPTCAPSOURCE_H
