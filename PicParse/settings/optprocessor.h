#ifndef OPTPROCESSOR_H
#define OPTPROCESSOR_H

#include <QString>
#include <QJsonArray>
#include <vector>
#include "opencv2/imgproc/imgproc.hpp"
#include "opt.h"
#include "optcascade.h"
#include "optrect.h"
#include "optsize.h"
#include "regionsettings.h"

class OptCascadeRotations
{
public:
    int maxAngle;
    int angleStep;

    std::vector<OptCascade> cascades;

    bool operator==(OptCascadeRotations &other);
};

class OptProcessor : public Opt
{
public:
    OptProcessor();

    int parserCount;
    double minFaceOverlap;

    bool keepFullWc;
    bool keepFullTv;
    bool keepWhenNoFaces;

    QString tvRectUnifPath;
    RegionSettings regionSettings;

    OptSize scaleTv;
    OptSize scaleWc;

    std::vector<OptCascadeRotations> rotations;
    std::vector<OptCascade> cascadesPostproces;

    bool setJSON(QJsonValue value);
    QJsonValue getJSON();
    void setDefaults();

    bool operator==(OptProcessor &other);
    inline bool operator!=(OptProcessor &other){return !(*this == other);}

};

#endif // OPTPROCESSOR_H
