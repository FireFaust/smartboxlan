#include "optprocessor.h"

OptProcessor::OptProcessor()
{
    setDefaults();
}

void OptProcessor::setDefaults()
{
    OptCascadeRotations tRotation;

    parserCount = 2;
    minFaceOverlap = 0.2;
    keepFullWc = true;
    keepFullTv = true;
    keepWhenNoFaces = true;
    rotations.clear();
    tRotation.angleStep = 18;
    tRotation.maxAngle = 90;
    tRotation.cascades.push_back(OptCascade());
    rotations.push_back(tRotation);
    cascadesPostproces.clear();
}

bool OptProcessor::setJSON(QJsonValue value)
{
    QJsonValue tVal,tVal2;
    QJsonArray tArr, tArr2;
    QJsonObject tObj;
    OptCascade tCascade;
    OptCascadeRotations tRotation;
    bool set;

    if(!value.isObject()){
        qWarning("Opt: Invalid parser type - must be object");
        return false;
    }
    QJsonObject opt = value.toObject();
    //=============================================================== parser_count
    if(!getInt(opt,parserCount,"parser_count")) return false;
    //=============================================================== min_face_overlap
    if(!getDouble(opt,minFaceOverlap,"min_face_overlap")) return false;
    //=============================================================== rotations
    if(!getArray(opt,tArr,"rotations",true)) return false;
    rotations.clear();
    for(int i = 0; i < tArr.size(); i++)
    {
        tVal = tArr[i];
        if(!tVal.isObject()){
            qWarning("Opt: Invalid type for rotations child - must be object (hint: check for trailing comas)");
            return false;
        }
        tObj = tVal.toObject();
        tRotation = OptCascadeRotations();
        //=============================================================== max_angle
        if(!getInt(tObj,tRotation.maxAngle,"max_angle")) return false;
        //=============================================================== angle_step
        if(!getInt(tObj,tRotation.angleStep,"angle_step")) return false;
        //=============================================================== cascades
        if(!getArray(tObj,tArr2,"cascades",true)) return false;
        if(tArr2.size() < 1){
            qWarning("Opt: At least 1 cascade must be defined!");
            return false;
        }
        for(int i2 = 0; i2 < tArr2.size(); i2++)
        {
            tVal2 = tArr2[i2];
            if(!tVal2.isObject()){
                qWarning("Opt: Invalid type for cascade - must be object (hint: check for trailing comas)");
                return false;
            }
            tCascade = OptCascade(tVal2);
            tRotation.cascades.push_back(tCascade);
        }
        rotations.push_back(tRotation);
    }
    //=============================================================== cascades_postproces
    if(!getArray(opt,tArr,"cascades_postproces",true,set)) return false;
    if(set){
        cascadesPostproces.clear();
        for(int i = 0; i < tArr.size(); i ++)
        {
            tVal = tArr[i];
            if(!tVal.isObject()){
                qWarning("Opt: Invalid type for cascades_postproces item - must be object (hint: check for trailing comas)");
                return false;
            }
            tCascade = OptCascade(tVal);
            cascadesPostproces.push_back(tCascade);
        }
    }
    //=============================================================== region_settings
    if(!getOpt(opt,regionSettings,"region_settings")) return false;
    //=============================================================== tv_rect_unif_path
    if(!getString(opt,tvRectUnifPath,"tv_rect_unif_path")) return false;
    //=============================================================== scale_tv
    if(!getOpt(opt,scaleTv,"scale_tv")) return false;
    //=============================================================== scale_wc
    if(!getOpt(opt,scaleWc,"scale_wc")) return false;
    //=============================================================== keep_full_wc
    if(!getBool(opt,keepFullWc,"keep_full_wc")) return false;
    //=============================================================== keep_full_tv
    if(!getBool(opt,keepFullTv,"keep_full_tv")) return false;
    //=============================================================== keep_when_no_faces
    if(!getBool(opt,keepWhenNoFaces,"keep_when_no_faces")) return false;
    return true;
}

QJsonValue OptProcessor::getJSON()
{
    QJsonObject tObj,tObj2;
    QJsonArray tArr,tArr2;

    tObj.insert("parser_count",parserCount);
    tObj.insert("min_face_overlap",minFaceOverlap);

    tArr = QJsonArray();
    for(unsigned int i = 0; i < rotations.size(); i++)
    {
        tObj2 = QJsonObject();
        tObj2.insert("max_angle",rotations[i].maxAngle);
        tObj2.insert("angle_step",rotations[i].angleStep);
        tArr2 = QJsonArray();
        for(unsigned int i2 = 0; i2 < rotations[i].cascades.size(); i2++)
        {
            tArr2.push_back(rotations[i].cascades[i2].getJSON());
        }
        tObj2.insert("cascades",tArr2);
        tArr.push_back(tObj2);

    }
    tObj.insert("rotations",tArr);
    tArr = QJsonArray();
    for(unsigned int i = 0; i < cascadesPostproces.size(); i++)
    {
        tArr.push_back(cascadesPostproces[i].getJSON());
    }
    tObj.insert("cascades_postproces",tArr);

    tObj.insert("keep_full_wc",QString(keepFullWc?"true":"false"));
    tObj.insert("keep_full_tv",QString(keepFullTv?"true":"false"));
    tObj.insert("keep_when_no_faces",QString(keepWhenNoFaces?"true":"false"));
    tObj.insert("region_settings",regionSettings.getJSON());
    tObj.insert("tv_rect_unif_path",tvRectUnifPath);
    tObj.insert("scale_tv",scaleTv.getJSON());
    tObj.insert("scale_wc",scaleWc.getJSON());

    return QJsonValue(tObj);
}

bool OptProcessor::operator== (OptProcessor &other){
    if(tvRectUnifPath != other.tvRectUnifPath) return false;
    if(regionSettings != other.regionSettings) return false;
    if(scaleTv != other.scaleTv) return false;
    if(scaleWc != other.scaleWc) return false;
    if(keepFullWc != other.keepFullWc) return false;
    if(keepFullTv != other.keepFullTv) return false;
    if(keepWhenNoFaces != other.keepWhenNoFaces) return false;
    if(parserCount != other.parserCount) return false;
    if(minFaceOverlap != other.minFaceOverlap) return false;
    if(rotations.size() != other.rotations.size() || !std::equal( rotations.begin(), rotations.end(), other.rotations.begin())) return false;
    if(cascadesPostproces.size() != other.cascadesPostproces.size() || !std::equal( cascadesPostproces.begin(), cascadesPostproces.end(), other.cascadesPostproces.begin())) return false;
    return true;
}

bool OptCascadeRotations::operator==(OptCascadeRotations &other){
    if(maxAngle != other.maxAngle) return false;
    if(angleStep != other.angleStep) return false;
    if(!std::equal( cascades.begin(), cascades.end(), other.cascades.begin())) return false;
    return true;
}
