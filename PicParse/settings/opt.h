#ifndef OPT_H
#define OPT_H

#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QJsonDocument>

#define EMPTY_JSON_OBJ_STRING "========EMPTY=======empty======="

class Opt
{
public:
    Opt();
    virtual bool setJSON(QJsonValue value) = 0;
    virtual QJsonValue getJSON() = 0;
    virtual void setDefaults() = 0;
    bool dumpToFile(QString filename);
    bool readFromFile(QString filename);
    QByteArray toByteArray();
    bool fromByteArray(QByteArray data);

protected:

    bool getOpt(
            QJsonObject src,
            Opt &value,
            QString fieldName,
            bool required = false,
            QString invalidMsg = "Opt: invalid type for %1! Must be object!"
            );
    bool getInt(
            QJsonObject src,
            int &value,
            QString fieldName,
            bool required = false,
            QString invalidMsg = "Opt: invalid type for %1! Must be int!"
            );
    bool getDouble(
            QJsonObject src,
            double &value,
            QString fieldName,
            bool required = false,
            QString invalidMsg = "Opt: invalid type for %1! Must be double!"
            );
    bool getString(
            QJsonObject src,
            QString &value,
            QString fieldName,
            bool required = false,
            QString invalidMsg = "Opt: invalid type for %1! Must be string!"
            );
    bool getObject(
            QJsonObject src,
            QJsonObject &value,
            QString fieldName,
            bool required = false,
            QString invalidMsg = "Opt: invalid type for %1! Must be object!"
            );
    bool getArray(
            QJsonObject src,
            QJsonArray &value,
            QString fieldName,
            bool required = false,
            QString invalidMsg = "Opt: invalid type for %1! Must be array!"
            );
    bool getBool(
            QJsonObject src,
            bool &value,
            QString fieldName,
            bool required = false,
            QString invalidMsg = "Opt: invalid type for %1! Must be string or number true=[true|TRUE|yes|YES|1] false=other values!"
            );
    //======================================================================
    bool getOpt(
            QJsonObject src,
            Opt &value,
            QString fieldName,
            bool required,
            bool &set,
            QString invalidMsg = "Opt: invalid type for %1! Must be object!"
            );
    bool getInt(
            QJsonObject src,
            int &value,
            QString fieldName,
            bool required,
            bool &set,
            QString invalidMsg = "Opt: invalid type for %1! Must be int!"
            );
    bool getDouble(
            QJsonObject src,
            double &value,
            QString fieldName,
            bool required,
            bool &set,
            QString invalidMsg = "Opt: invalid type for %1! Must be double!"
            );
    bool getString(
            QJsonObject src,
            QString &value,
            QString fieldName,
            bool required,
            bool &set,
            QString invalidMsg = "Opt: invalid type for %1! Must be string!"
            );
    bool getObject(
            QJsonObject src,
            QJsonObject &value,
            QString fieldName,
            bool required,
            bool &set,
            QString invalidMsg = "Opt: invalid type for %1! Must be object!"
            );
    bool getArray(
            QJsonObject src,
            QJsonArray &value,
            QString fieldName,
            bool required,
            bool &set,
            QString invalidMsg = "Opt: invalid type for %1! Must be array!"
            );
    bool getBool(
            QJsonObject src,
            bool &value,
            QString fieldName,
            bool required,
            bool &set,
            QString invalidMsg = "Opt: invalid type for %1! Must be string or number true=[true|TRUE|yes|YES|1] false=other values!"
            );
private:

    bool contains(
            QJsonObject src,
            QString fieldName,
            bool required = false,
            QString requiredMsg = "Opt: required field %1 missing!"
            );
};

#endif // OPT_H
