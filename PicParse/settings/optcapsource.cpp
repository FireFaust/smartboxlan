#include "optcapsource.h"

OptCapSource::OptCapSource()
{
    setDefaults();
}

void OptCapSource::setDefaults()
{
    sourceType = OPENCV_CAP;
    sourceNum = -1;
    filename = "";
}

bool OptCapSource::setJSON(QJsonValue value)
{
    QJsonValue tVal;
    QString tStr;
    int tInt;

    if(!value.isObject()){
        qWarning("Opt: Invalid cap source type");
        return false;
    }
    QJsonObject opt = value.toObject();
    if(!opt.contains("type")){
        qWarning("Opt: Cap source must contain type");
        return false;
    }
    tVal = opt["type"];
    if(!tVal.isString()){
        qWarning("Opt: Cap source type must be a string");
        return false;
    }
    tStr = opt["type"].toString();
    if(tStr == "opencv"){
        sourceType = OPENCV_CAP;
    } else if(tStr == "flycap"){
        sourceType = FLYCAP_CAP;
    } else if(tStr == "video"){
        sourceType = VIDEO_CAP;
    } else if(tStr == "easycap"){
        sourceType = EASY_CAP;
    } else {
        qWarning() << "Opt: Cap source type unsupported must be: opencv|flycap|video|easycap!";
        return false;
    }

    if(opt.contains("source")) switch(sourceType){
    case OPENCV_CAP:
    case EASY_CAP:
    case FLYCAP_CAP:
        tVal = opt["source"];
        if(!tVal.isDouble()){
            qWarning("Opt: Cap source.source must be numeric for type opencv|flycap!");
            return false;
        }
        tInt = (int)tVal.toDouble();
        if(tInt < -1){
            qWarning("Opt: Cap source.source must be greater than -1 for type opencv|flycap!");
            return false;
        }
        sourceNum = tInt;
        break;
    case VIDEO_CAP:
        tVal = opt["source"];
        if(!tVal.isString()){
            qWarning("Opt: Cap source.source must be string for type video!");
            return false;
        }
        tStr = tVal.toString();
        if(!QFile::exists(tStr)){
            qWarning() << "Opt: Cap source.source file [" << tStr.toStdString().c_str() << "] does not exist!";
            return false;
        }
        filename = tStr;
        break;
    }

    return true;
}

QJsonValue OptCapSource::getJSON()
{
    QJsonObject tObj;
    switch(sourceType){
    case OPENCV_CAP:
        tObj.insert("type",QString("opencv"));
        break;
    case FLYCAP_CAP:
        tObj.insert("type",QString("flycap"));
        break;
    case VIDEO_CAP:
        tObj.insert("type",QString("video"));
        break;
    case EASY_CAP:
        tObj.insert("type",QString("easycap"));
        break;
    }
    switch(sourceType){
    case OPENCV_CAP:
    case EASY_CAP:
    case FLYCAP_CAP:
        tObj.insert("source",sourceNum);
        break;
    case VIDEO_CAP:
        tObj.insert("source",filename);
        break;
    }
    return QJsonValue(tObj);
}

bool OptCapSource::operator==(OptCapSource &other){
    if(sourceType != other.sourceType) return false;
    if(sourceNum != other.sourceNum) return false;
    if(filename != other.filename) return false;
    return true;
}



