#include "optsize.h"

OptSize::OptSize()
{
    setDefaults();
}

OptSize::OptSize(int width, int height)
{
    this->width = width;
    this->height = height;
}

void OptSize::setDefaults()
{
    width=height = 0;
}

bool OptSize::setJSON(QJsonValue value)
{
    if(!value.isObject()){
        qWarning("Opt: Invalid type - must be object for rectangle");
        return false;
    }
    QJsonObject opt = value.toObject();
    //=============================================================== width
    if(!getInt(opt,width,"width")) return false;
    //=============================================================== height
    if(!getInt(opt,height,"height")) return false;
    return true;
}

QJsonValue OptSize::getJSON()
{
    QJsonObject tObj;
    tObj.insert("width",width);
    tObj.insert("height",height);
    return QJsonValue(tObj);
}

bool OptSize::operator==(OptSize &other){
    if(width != other.width) return false;
    if(height != other.height) return false;
    return true;
}
