#ifndef FACELEARN_H
#define FACELEARN_H

#include <QDebug>
#include <vector>
#include <deque>

#include <QByteArray>

#include "opencv2/contrib/contrib.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "smartconv.h"

class FaceLearn
{
public:
    FaceLearn();
    ~FaceLearn();
    cv::Ptr<cv::FaceRecognizer> model;

    void saveToFile(QString filename);
    double addFace(QByteArray img);
    double addFace(cv::Mat * img);
    std::vector<QByteArray *> getFaces();

    void train();

    void init();
    void clear();
    inline int getTrainedFaces(){return trainedFaces;}


private:

    int trainedFaces;
    unsigned int stdDevHistoryMax;
    //double stdDevMax;
    double avgStdDevMax;
    double minStdPercent;
    int stdPercentMul;


    std::vector<cv::Mat *> originalFaces;
    std::vector<cv::Mat *> scaledFaces;

    std::deque<double> stdDevArr;
    std::deque<double> stdDevAvgArr;

    inline void addDev(double val){ if(!stdDevArr.empty() && stdDevArr.size() >= stdDevHistoryMax) stdDevArr.pop_front(); stdDevArr.push_back(std::min(val,100.));}
    inline double stdDev(double val){return std::min(100.,std::abs(avgStdDev()-val));}
    double avgStdDev();

    inline void addAvgDev(double val){ if(!stdDevAvgArr.empty() && stdDevAvgArr.size() >= stdDevHistoryMax) stdDevAvgArr.pop_front(); stdDevAvgArr.push_back(std::min(val,100.));}
    inline double avgStdDev(double val){return std::min(100.,std::abs(avgAvgStdDev()-val));}
    double avgAvgStdDev();

};

#endif // FACELEARN_H
