#include "picparse.h"

#define PI 3.14159265

PicParse::PicParse(SmartSettings settings)
{
    initSettings(settings);
    init();
}

PicParse::PicParse(SmartSettings settings, bool debug)
{
    initSettings(settings);
    init();
    this->debug = debug;
}

void PicParse::init()
{
    debug = false;
    flip_mat = 0;
    rot_mat = 0;
    flip_rot_mat_def_contents = cv::Scalar(0);
}

PicParse::~PicParse()
{
    if(rot_mat != 0){
        /// Deletes rotation cv::Mat if neccecary
        // rot_mat->release();
        delete rot_mat;
    }
}

bool PicParse::initSettings(SmartSettings settings)
{
    this->settings = settings;
    rotations.clear();
    cascadesPostproces.clear();
    faces.setMinFaceOverlap(settings.processor.minFaceOverlap);
    for( unsigned int r = 0; r < settings.processor.rotations.size(); r++ ){
        ParseRotation rotation;
        rotation.angleStep = settings.processor.rotations[r].angleStep;
        rotation.maxAngle = settings.processor.rotations[r].maxAngle;
        for(unsigned int c = 0; c < settings.processor.rotations[r].cascades.size(); c++ ){
            ParseCascade cascade;
            if(!cascade.fromOpt(settings.processor.rotations[r].cascades[c])) continue;

            rotation.cascades.push_back(cascade);
        }

        rotations.push_back(rotation);
    }
    for(unsigned int c = 0; c < settings.processor.cascadesPostproces.size(); c++ ){
        ParseCascade cascade;
        if(!cascade.fromOpt(settings.processor.cascadesPostproces[c])) continue;
        cascadesPostproces.push_back(cascade);
    }
    return true;
}

void PicParse::RotateFrame(cv::Mat src, cv::Mat &dst, double angle)
{
    cv::Mat rot_mat( 2, 3, dst.type());
    cv::Mat off_mat = (cv::Mat_<double>(3,3) <<
                       1, 0, (dst.size().width - src.size().width)/2,
                       0, 1, (dst.size().height - src.size().height)/2,
                       0, 0, 1);
    /** Rotating the image after Warp */
    /// Compute a rotation matrix with respect to the center of the image
    cv::Point center = cv::Point( dst.cols/2, dst.rows/2 );
    /// Get the rotation matrix with the specifications above
    rot_mat = getRotationMatrix2D( center, angle, 1 );
    /// Rotate the image
    warpAffine( src, dst, rot_mat * off_mat, dst.size() );
    //dst.copyTo(dst);
    off_mat.release();
}

cv::Point PicParse::RotatePoint(cv::Point center, cv::Point point, double angle)
{
    cv::Point rez;
    float s = sin(angle*PI/180);
    float c = cos(angle*PI/180);
    point.x -= center.x;
    point.y -= center.y;
    rez.x = (float)point.x * c - (float)point.y * s;
    rez.y = (float)point.x * s + (float)point.y * c;
    rez.x += center.x;
    rez.y += center.y;
    return rez;
}

cv::Rect PicParse::ContainedRotatedRect(cv::Rect source, cv::Point center, double angle)
{
    cv::Point tP;
    int minx,miny,maxx,maxy;
    tP.x = source.x;
    tP.y = source.y;
    tP = RotatePoint(center,tP,angle);
    minx = maxx = tP.x;
    miny = maxy = tP.y;

    tP.x = source.x+source.width;
    tP.y = source.y+source.height;
    tP = RotatePoint(center,tP,angle);
    minx = std::min(tP.x,minx);
    maxx = std::max(tP.x,maxx);
    miny = std::min(tP.y,miny);
    maxy = std::max(tP.y,maxy);

    tP.x = source.x;
    tP.y = source.y+source.height;
    tP = RotatePoint(center,tP,angle);
    minx = std::min(tP.x,minx);
    maxx = std::max(tP.x,maxx);
    miny = std::min(tP.y,miny);
    maxy = std::max(tP.y,maxy);

    tP.x = source.x+source.width;
    tP.y = source.y;
    tP = RotatePoint(center,tP,angle);
    minx = std::min(tP.x,minx);
    maxx = std::max(tP.x,maxx);
    miny = std::min(tP.y,miny);
    maxy = std::max(tP.y,maxy);

    return cv::Rect(minx,miny,maxx-minx,maxy-miny);
}

void PicParse::ParseFaces(cv::Mat &img)
{
    if(img.empty()) return;
    bool is_img_cloned = false; //!< True if cv::Mat * source should be released at the end;

    cv::Mat * source = 0, * rotated = 0;
    cv::Point source_center;
    cv::Point rot_mat_offset, * flip_rot_mat_center;
    int t_edge;

    /// Convert cv::Mat *img to grayscale/CV_8U if neccecary
    if(img.type() != CV_8UC1){
        is_img_cloned = true;
        source = new cv::Mat(img.size(),CV_8UC1);
        cv::cvtColor( img, *source, cv::COLOR_BGR2GRAY );
    }else{
        source = &img;
    }

    /// Calculates maximum dimensions of rotation cv::Mat
    //int maxSize = std::sqrt(std::pow(source->size().width,2)+std::pow(source->size().height,2));
    int maxSize = std::max(source->size().width,source->size().height);
    cv::Size t_rot_mat_size(maxSize,maxSize);

    /// Creates mat for rotation with any step, but with large black areas
    if(rot_mat == 0){
        /// Creates rotation cv::Mat for the first time if neccecary
        rot_mat_size = t_rot_mat_size;
        if(rot_mat != 0) delete rot_mat;
        rot_mat = new cv::Mat(rot_mat_size,CV_8UC1, flip_rot_mat_def_contents);
        rot_mat_center = cv::Point(rot_mat_size.width/2, rot_mat_size.height/2);
    }else if(t_rot_mat_size != rot_mat_size){
        /// Or re-creates rotation cv::Mat if sizes does not match
        delete rot_mat;
        rot_mat_size = t_rot_mat_size;
        rot_mat = new cv::Mat(rot_mat_size,CV_8UC1, flip_rot_mat_def_contents);
        rot_mat_center = cv::Point(rot_mat_size.width/2, rot_mat_size.height/2);
    }

    /// Creates mat for rotation with 90' step
    cv::Size t_flip_mat_size(source->size().height,source->size().width);
    if(flip_mat == 0){
        /// Creates rotation cv::Mat for the first time if neccecary
        flip_mat_size = t_flip_mat_size;
        flip_mat = new cv::Mat(flip_mat_size,CV_8UC1, flip_rot_mat_def_contents);
        flip_mat_center = cv::Point(flip_mat_size.width/2,flip_mat_size.height/2);
    }else if(t_flip_mat_size != flip_mat_size){
        /// Or re-creates rotation cv::Mat if sizes does not match
        delete flip_mat;
        flip_mat_size = t_rot_mat_size;
        flip_mat = new cv::Mat(flip_mat_size,CV_8UC1, flip_rot_mat_def_contents);
        flip_mat_center = cv::Point(flip_mat_size.width/2,flip_mat_size.height/2);
    }

    //cv::equalizeHist( *source, *source );
    /// Clear previous faces
    faces.clear();

    /// Find center of source img
    source_center.x = source->cols/2;
    source_center.y = source->rows/2;

    for(std::vector<ParseRotation>::iterator r = rotations.begin(); r != rotations.end(); ++r)
    {
        for(int angle = -(*r).maxAngle; angle <= (*r).maxAngle; angle += (*r).angleStep)
        {
            rot_mat_offset.x = rot_mat_offset.y = 0;

            switch(angle){
            case 0:
                rotated = source;
                flip_rot_mat_center = &source_center;
                break;
            case 90:
                cv::transpose(*source, *flip_mat);
                cv::flip(*flip_mat,*flip_mat,0);
                flip_mat->copyTo(*flip_mat);
                rotated = flip_mat;
                t_edge = (source->size().width-source->size().height)/2;
                rot_mat_offset.x = -t_edge;
                rot_mat_offset.y = t_edge;
                flip_rot_mat_center = &flip_mat_center;
                break;
            case -90:
                cv::transpose(*source, *flip_mat);
                cv::flip(*flip_mat,*flip_mat,1);
                flip_mat->copyTo(*flip_mat);
                rotated = flip_mat;
                t_edge = (source->size().width-source->size().height)/2;
                rot_mat_offset.x = -t_edge;
                rot_mat_offset.y = t_edge;
                flip_rot_mat_center = &flip_mat_center;
                break;
            default:
                *rot_mat = flip_rot_mat_def_contents;
                RotateFrame(*source,*rot_mat, angle);
                rotated = rot_mat;
                rot_mat_offset.x = (rotated->size().width - source->size().width) / 2;
                rot_mat_offset.y = (rotated->size().height - source->size().height) / 2;
                flip_rot_mat_center = &rot_mat_center;
            }

            for(std::vector<ParseCascade>::iterator c = (*r).cascades.begin(); c != (*r).cascades.end(); ++c)
            {
                std::vector<cv::Rect> tFaces;
                cv::Rect tRect;
                if(!(*c).hasMaxSize)
                {
                    (*c).classifier.detectMultiScale( *rotated, tFaces, (*c).scaleStep, (*c).minNeighbors,(*c).flags,(*c).minSize);
                }else{
                    (*c).classifier.detectMultiScale( *rotated, tFaces, (*c).scaleStep, (*c).minNeighbors,(*c).flags,(*c).minSize,(*c).maxSize);
                }
                for(unsigned i = 0;i < tFaces.size();i++){
                    tRect = ContainedRotatedRect(tFaces[i],*flip_rot_mat_center,angle);
                    tRect.x -= rot_mat_offset.x;
                    tRect.y -= rot_mat_offset.y;
                    faces.AddFaceFromMat(rotated,tFaces[i],tRect,angle);
                    //cv::rectangle( *rotated, tFaces[i], cv::Scalar( 255 ));
                }
            }
        }


    }

    if(!cascadesPostproces.empty()){
        for(std::vector<ParsedFace>::iterator f = faces.faces.begin(); f != faces.faces.end();)
        {
            bool found = false;
            for(std::vector<ParseCascade>::iterator c = cascadesPostproces.begin(); c != cascadesPostproces.end(); ++c)
            {
                std::vector<cv::Rect> tFaces;
                if(!(*c).hasMaxSize)
                {
                    (*c).classifier.detectMultiScale( (*f).img, tFaces, (*c).scaleStep, (*c).minNeighbors,(*c).flags,(*c).minSize);
                }else{
                    (*c).classifier.detectMultiScale( (*f).img, tFaces, (*c).scaleStep, (*c).minNeighbors,(*c).flags,(*c).minSize,(*c).maxSize);
                }
                if(tFaces.size() > 0){
                    found = true;
                    break;
                }
            }
            if(!found){
                // qDebug() << "Postprocess face removed";
                faces.faces.erase(f);
            }else{
                ++f;
                //  qDebug() << "Postprocess face untouched";
            }
        }
    }
    //*img = rotated.clone();
    //cv::cvtColor(*img, merged, CV_BGR2GRAY);

    if(debug){
        for( unsigned i = 0; i < faces.faces.size(); i++ )
        {
            cv::rectangle( img, faces.faces[i].position, cv::Scalar( 0 ),5);
        }
    }

    if(is_img_cloned){
        /// Cleanup grayscale cv::Mat (clone of img)
        delete source;
    }
    //img = *rotated;
}


void PicParse::DumpFaces(QDir dir, QString filename)
{
    if(faces.size() == 0) return;
    if(!dir.exists()){
        if(!dir.mkpath(".")){
            qCritical() << "Could not create directory for dumping faces:" << dir.path().toUtf8().constData();
            return;
        }
    }

    QByteArray * data = 0;

    for(unsigned int i = 0; i < faces.size(); i++)
    {
        try
        {

            QFile file(dir.filePath(QString(filename).arg(i)).toStdString().c_str());
            file.open(QIODevice::WriteOnly);
            data = SmartConv::MatToJpgP(faces[i].img);
            file.write(*data);
            file.close();
            delete data;
            /*
            cv::imwrite(dir.filePath(QString(filename).arg(i)).toStdString().c_str(), faces[i].img, params);
            */
        }
        catch (std::runtime_error& ex)
        {
            qCritical() << "Could not save face img: "<< ex.what();
            return;
        }
    }
}

std::vector<cv::Mat> PicParse::GetFaces()
{
    std::vector<cv::Mat> result;
    if(faces.size() == 0) return result;
    for(unsigned int i = 0; i < faces.size(); i++)
        result.push_back(faces[i].img);
    return result;
}

std::vector<QByteArray*> PicParse::GetFacesJpg()
{
    std::vector<QByteArray*> result;
    if(faces.size() == 0) return result;
    for(unsigned int i = 0; i < faces.size(); i++)
        result.push_back(SmartConv::MatToJpgP(faces[i].img));
    return result;
}

int PicParse::faceCount()
{
    return faces.size();
}

cv::Mat * PicParse::getPart(cv::Mat &img, cv::Rect rect)
{
    cv::Mat * result;
    if(rect.x > img.cols || rect.y > img.rows) return new cv::Mat();
    if(rect.x + rect.width > img.cols) rect.width = img.cols - rect.x;
    if(rect.y + rect.height > img.rows) rect.height = img.rows - rect.y;
    result = new cv::Mat(rect.size(),CV_8UC1);
    img(rect).copyTo(*result);
    return result;
}

QByteArray * PicParse::getPartB(cv::Mat &img, cv::Rect rect)
{
    if(rect.area() == 0) return 0;
    cv::Mat * mat = getPart(img,rect);
    QByteArray * result;
    if(mat->empty()){
        delete mat;
        return 0;
    }
    result =  SmartConv::MatToJpgP(*mat);
    delete mat;
    return result;
}



QByteArray * PicParse::getTvPart(cv::Mat &img)
{
    return getPartB(img,settings.processor.regionSettings.channelRect.rect());
}

QByteArray * PicParse::getTvPartUnif(cv::Mat &img)
{
    return getPartB(img,settings.processor.regionSettings.unifiedRect.rect());
}

QByteArray * PicParse::getPartJpeg(QByteArray * source,int x, int y, int w, int h)
{
    cv::Mat * img = SmartConv::JpgToMatP(source);
    cv::Mat * result;
    cv::Rect rect(x,y,w,h);
    if(rect.x > img->cols || rect.y > img->rows) return new QByteArray();
    if(rect.x + rect.width > img->cols) rect.width = img->cols - rect.x;
    if(rect.y + rect.height > img->rows) rect.height = img->rows - rect.y;
    result = new cv::Mat(rect.size(),CV_8UC1);
    (*img)(rect).copyTo(*result);
    delete img;
    QByteArray * res = SmartConv::MatToJpgP(*result);
    delete result;
    return res;
}

QByteArray * PicParse::getScaledTv(cv::Mat &img, QByteArray * orig)
{
    if(settings.processor.scaleTv.size().area() == 0)
    {
        return orig;
    }
    cv::Size destSize = settings.processor.scaleTv.size();
    double ratioW = (double)destSize.width/img.cols, ratioH = (double)destSize.height/img.rows;
    if(ratioW<ratioH){
        destSize.width = ratioW*img.cols; destSize.height = ratioW*img.rows;
    }else{
        ratioW = ratioH;
        destSize.width = ratioW*img.cols; destSize.height = ratioW*img.rows;
    }
    cv::Mat * dest = new cv::Mat(destSize,CV_8UC1);
    cv::resize(img,*dest,destSize);
    QByteArray * res = SmartConv::MatToJpgP(*dest);
    delete dest;
    return res;
}

QByteArray * PicParse::getScaledWc(cv::Mat &img, QByteArray * orig)
{
    if(settings.processor.scaleWc.size().area() == 0)
    {
        return orig;
    }
    cv::Size destSize = settings.processor.scaleWc.size();
    double ratioW = (double)destSize.width/img.cols, ratioH = (double)destSize.height/img.rows;
    if(ratioW<ratioH){
        destSize.width = ratioW*img.cols; destSize.height = ratioW*img.rows;
    }else{
        ratioW = ratioH;
        destSize.width = ratioW*img.cols; destSize.height = ratioW*img.rows;
    }
    cv::Mat * dest = new cv::Mat(destSize,CV_8UC1);
    cv::resize(img,*dest,destSize);
    QByteArray * res = SmartConv::MatToJpgP(*dest);
    delete dest;
    return res;
}
