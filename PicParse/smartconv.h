#ifndef SMARTCONV_H
#define SMARTCONV_H

#include <QDebug>
#include <QByteArray>
#include "opencv2/imgproc/imgproc.hpp"

#include "jpeglib.h"

class SmartConv
{
public:
    SmartConv();

    static QByteArray MatToJpg(cv::Mat src, int quality = 100);
    static QByteArray * MatToJpgP(cv::Mat src, int quality = 100);
    static cv::Mat * JpgToMatP(QByteArray * src);
};

#endif // SMARTCONV_H
