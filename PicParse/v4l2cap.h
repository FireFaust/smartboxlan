#ifndef V4L2CAP_H
#define V4L2CAP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "stdlib.h"
#include <stdio.h>


#include <getopt.h>             /* getopt_long() */

#include <fcntl.h>              /* low-level i/o */
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <time.h>

#include <linux/videodev2.h>

#include <QDebug>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

enum io_method {
        IO_METHOD_READ,
        IO_METHOD_MMAP,
        IO_METHOD_USERPTR,
};

struct buffer {
        void   *start;
        size_t  length;
};

class V4L2Cap
{
public:
    V4L2Cap(int mDevId = -1);
    ~V4L2Cap();

    char * grab();
    inline void setAutoReload(bool autoReload){this->autoReload = autoReload;}
    inline bool isOk(){return ok;}
    bool reload();
    const char * error;
    inline int getWidth(){return width;}
    inline int getHeight(){return height;}

private:
    void release();

    //Init
    bool init();
    bool initRead(unsigned int buffer_size);
    bool initMmap();
    bool initUserp(unsigned int buffer_size);
    void getFmt();
    //Grab
    bool getFrame();

    bool ok;
    bool anyDevice;
    bool autoReload;
    bool fixedDevice;
    char * dev_name;
    int width;
    int height;
    char * grayscale;
    io_method io;
    unsigned int n_buffers;
    bool mustDealocate;
    __u32 pixelformat;

    int devId;
    int fd;
    float retryTimeout;

    struct buffer *buffers;
    int xioctl(int fh, int request, void *arg);
    bool err(const char * msg);
    bool convertYuvy2Y(const void *src, int maxSize);
};

#endif // V4L2CAP_H
