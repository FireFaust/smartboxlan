#-------------------------------------------------
#
# Project created by QtCreator 2014-01-05T12:30:46
#
#-------------------------------------------------

QT       -= gui

TARGET = SmbCap
TEMPLATE = lib
CONFIG += staticlib

SOURCES += smbcap.cpp

HEADERS += smbcap.h
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
